:title: Introduction to Network Programming
:data-transition-duration: 1500
:css: networking.css

====================================
Layer 2 
====================================

Basics of Layer 2 and Ethernet

#######################

====================================
Layer 2 Overview  
====================================

- Introduction to Ethernet

- Layer 1 Network Devices

- Layer 2 Network Devices

- MAC Addresses

- Switching

- Ethernet Header

- Ethernet Header Fields

- ARP and RFC 826

- ARP header fields

- LAB

#######################

====================================
Introduction to Ethernet
====================================

Ethernet is a family of technologies used to connect LANs and WANs. It has used several different physical mediums over time from coaxial, to twisted pair, and now fiber. 

The Ethernet protocol is the foundation of sending and receiving traffic and is lowest level of network communication we will discuss in class. 

#######################

====================================
Synonyms
====================================

Layer 2 == Link Layer == MAC == Ethernet

"link" implies anyone that can be reached on layer 2 (e.g. everyone who recieives a L2 broadcast). This is almost always your whole local LAN

This is why we have "link scope" in IPv6. A link scope address is only useful within the local LAN. We will get to IPv6 in the next section

#######################

====================================
Layer 1 Devices
====================================

Sole-purpose layer 1 devices are mostly extinct in today's environments. As technology advanced, the functionality became obsolete or was rolled into layer 2 devices. 

Repeater - Has 2 ports. Takes recieved data from one side and retransmits it on the other. It allows the physical transmission medium to be extended.

Hub - A multi-port repeater. Data is recieved on one port, and retransmitted out ALL other ports (except port it was recieved on)

Single Collision Domain

Single Broadcast Domain

#######################

====================================
Layer 2 Devices
====================================

Bridges have two ports and connect two independent PHYSICAL networks. These days, you are most likely to encounter a bridge in virtualization software or a system's network interfaces. (e.g. bridging vs NAT mode for VMs, or combining two interfaces in Linux )

Switches are multiport bridges and are still prelevant in networks today. Switches are typically the lowest level device that makes decisions on where to send traffic. The MAC (Media Access Control) address is what switches use to make those decisions.

Multiple Collision Domain
  - Each physical connection to a switch port is a collision domain

Single Broadcast Domain 

#######################

====================================
MAC Addresses
====================================

48 bits long, usually represented as 6 groups of hex (e.g. aa:bb:cc:dd:ee:ff)

Manufacturers 'burn in' a MAC address to each port on a device, however most modern equipment allows the MAC to be changed in software as well

Special MAC Addresses

  - The Layer 2 BROADCAST address for IPv4 and ARP is FF:FF:FF:FF:FF:FF
  - The Layer 2 MULTICAST address for IPv6 is 33:33:00:00:00:01

#######################

====================================
MAC Addresses Breakdown
====================================

Organizational Unique Identifier - (OUI) First 3 bytes
 - OUIs specific to each manufacturer (VMWare OUI 00:50:56, Intel OUIs 00:02:B3, 00:03:47, 00:04:23)
 - LSBit in 1st byte in OUI determines if address is unicast (0) or muilticast (1)
 - 2nd LSBit in 1st byte in OUI determines if the mac is globally unique (0), or locally administered (1)


Host ID - Last 3 bytes
 - Randomly decided by manufacturer
 - Ideally there are no collisions within a physical network

#######################

====================================
CAM Tables
====================================

Switches operate using MAC addresses found in the Ethernet header of the traffic. 

Switches have CAM (Content Addressable Memory) tables that map a physical port to MAC addresses
  - Switches populate a CAM table by looking at the Ethernet header and determining the SOURCE address of traffic arriving on that port
  - It is possible the table maps multiple MAC Addresses to the same physical port (e.g. Switch port is connected to a hub with multiple hosts)

#######################

====================================
Switching Decisions
====================================

Switches check the DESTINATION MAC address against the CAM table
  - If the address is mapped to a port, forward it out that port
  - If no mapping, forward it out ALL ports EXCEPT the one it was received on 
    - Traffic usually gets a response, and the MAC table will be updated normally when that traffic is seen

If the DESTINATION MAC is a broadcast address, then forward it out ALL ports EXCEPT the one it was received on 


#######################

====================================
Header diagrams
====================================

The header diagrams have the same format throughout my slides.

In networking, most data is just a series of headers appended together in a specific order. 

At the top, I show rectangles indicating the order of the headers. The current header being defined is highlighted in orange

When you do your labs, you will be simply appending headers together (Ethernet + ARP)

#######################

====================================
Ethernet header
====================================

.. image:: img/ethernet_header.png

#######################

====================================
Ethernet Header Fields
====================================

Source MAC Address -  Originator of the traffic. Always a host.

Destination MAC Address - The Ethernet address of the intended recipient. Must be in the same broadcast domain. May be unicast, broadcast, or multicast MAC address

Ethernet Type (aka EtherType) - The next header following the ethernet header. Or to put it another way, it specifies the ethernet payload
  - ARP, IPv4, IPv6, etc...
  - See references page for the list onf constants


#######################

====================================
ARP Header
====================================

Maps a known Layer 3 address to an UNKNOWN layer 2 address

A host asks everyone "who has this layer 3 address"? (request)

The owner responds "I do. Here is my Layer 2 address" (reply)

#######################

====================================
ARP Header
====================================
.. image:: img/ethernet_arp_request.png


#######################

====================================
ARP Header fields
====================================

Hardware Type - Identifies the layer 2 address protocol (Common values are 1 for Ethernet, 6 for 802.11 (wifi))

Protocol Type - Identifies the type of Layer 3 address protocol that needs to be mapped to an Ethernet address. 
  - Convieniently uses the same constants that can be used for the EtherType field in the Ethernet header. See references

Hardware Address Length - Size of Layer 2 address, in bytes, that was specified in Hardware type

Protocol Address Length - Size of Layer 3 address, in bytes, that was specified in Protocol Type


#######################

====================================
ARP Header fields
====================================

Op Code - 1 for Request, 2 for Reply. Other codes exist for protocols that make use of an Arp Header

Sender Hardware Address - Layer 2 adddress of the sender

Sender Protocol Address - Layer 3 address of the sender 

Target Hardware Address - The layer 2 address of whoever owns the Target protocol address. This is filled with 0's on a request, and filled in on the reply

Target Protocol Address - The layer 3 address we need to map to a layer 2 address. 

#######################

====================================
Helper Code
====================================

How to use helper code. 

::

  from raw_socket_helper import RawSocket

  ... Your Code Here ...

  interfaceName = "ens33"   # from ifconfig
  raw_socket = RawSocket(interfaceName)

  raw_socket.send(frame)          # Layer 2 has no checksums
  

#######################


====================================
Coding Suggestions
====================================

Your raw socket code will build on itself. The later labs implicitly require previous layers to be correct

It is a chore to debug if your code is disorganized and poorly structured. It also makes it very difficult for me to help you.

I recommend three things

1) Organize your code into sections, where each section deals with a specific header

2) One variable with a descriptive name, per field. Don't combine fields unless you absolutely have too (ie smaller than a byte, bit flags, etc) 

3) Comment your variables so you know what the values are and what they mean


#######################


====================================
Coding Suggestions
====================================

Look at the example below for an incomplete skeleton code example. You will add variables and values for the fields in each header.

::

  from raw_socket_helper import RawSocket

  # Constants
  mymac = "\x00\x0c\x29\xc9\x78\x14"
  myip = ...

  # Layer 2 Header
  dst = ...      # Destination MAC
  src = mymac    # Source MAC
  typ = ...  
  etherhdr = dst + src + typ

  # ARP Header
  HardwareType = ...
  ProtocolType = ...

  arphdr = HardwareType + ... + ....
  
  # Make the complete frame and send it
  frame = etherhdr + arp

  interfaceName = "ens33"   # from ifconfig
  raw_socket = RawSocket(interfaceName)
  
  # Makes it easy to find in wireshark
  for i in range(3):
      raw_socket.send(frame)          
      

#######################

====================================
Debugging
====================================

Wireshark is your networking debugger. If your code runs, but does not work, the first thing you should do is identify the traffic in Wireshark. 

Follow each field in the packet window and compare it to the variable in your code that corresponds to it. If your code is messy or you have bad variable names, this WILL be a chore to isolate. 

Generally wireshark will highlight errors in yellow or red and annotate them. If it doesn't, stepping through the packet window will show you exactly where you went wrong

NOTE: A correctly formatted packet may still have bad data, If your code doesn't work, and wireshark shows it as a valid packet, your actual values are likely incorrect

#######################

====================================
Common Errors
====================================

Common errors include:

  - there is a mismatch between the protocol length and the data you supplied
  - if there is a length field, it was incorrectly computed 
  - transposed bytes
  - incorrect widths (putting a 16 bit value in an 8 bit field. Or vice versa)
  - you have bogus headers
  - bad or missing checksum
  - Wrong value type (ip address scope, bits vs bytes, etc)

Invalid checksums can be caused by almost any error, including the ones above. 


#######################

====================================
Debugging Algorithm
====================================

This applies throughout the class

1) Did your code provide an error/stack trace? 

2-3) Read the RFC's closely. Did it specify a particular value or format?

4) Should you be using send() or send_chksum()?

5) Are you using the appropriate Address Family/Protocol Type?

6) Does your traffic show up in wireshark?

7) Does wireshark show any errors?

7b) If not, does Wireshark show all the data you expect to be there, in the expected order?

8) According to step 2 and 3, are you putting in the correct width data in each field? (e.g. 0x01 and 0x00 0x01 are different widths)

9) According to step 2 and 3, is the value in a length field calculated correctly? (the units may be in bytes, bits, or octets. It may encompass different things etc)

10) According to step 2 and 3, Is your value consistent with what the RFC and/or the lab instructions told you to put there? This includes standard values, IP address scopes, etc

Hint: Read the RFCs closely. 


#######################

====================================
LAB
====================================

In this lab you will be manually generating raw frames and transmitting them over the network.

Use the helper code provided to you. Put it in the same diectory as your code

#######################

====================================
LAB 2A
====================================

This lab is to validate that you can run your code, and network traffic shows on wireshark. 

Generate an Ethernet frame to your neighbor. They will have to tell you their MAC Address. Choose an arbitrary value for EtherType. 

Validate by seeing your traffic in wireshark.  For this specific lab, I don't care if Wireshark complains that it's wrong/bad/whatever. If you see traffic move on to 2B

#######################

====================================
LAB 2B
====================================

Generate a valid ARP Request to your neighbor. Valid means it conforms the RFC, Wireshark confirms it is not malformed/displays no errors, and your request produces a reply.

Ensure that you see the ARP Reply triggered by your ARP request in Wireshark. If you do not, your frame is incorrect.

What will happen if you send a complete ARP Reply to a host that did not send an ARP Request?
  - What vulnerability does this introduce?

#######################


====================================
References
====================================

RFC: 

  ARP - https://tools.ietf.org/html/rfc826

EtherType: 
  
  http://www.iana.org/assignments/ieee-802-numbers/ieee-802-numbers.xhtml


#######################

====================================
Layer 2 Overview  
====================================

Layer 1 and 2 Network devices
  - Switches are most prevelant

Layer 2 addresses are MAC addresses

Switching allows networking via Layer 2

Ethernet Header and ARP traffic
