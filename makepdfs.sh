#!/bin/bash

mkdir pdfs/
rst2pdf 00-concepts.rst pdfs/00-concepts.pdf
rst2pdf 01-bsdsockets.rst pdfs/01-bsdsockets.pdf
rst2pdf 02-layer2.rst pdfs/02-layer2.pdf
rst2pdf 03-layer3.rst pdfs/03-layer3.pdf
rst2pdf 04-layer4.rst pdfs/04-layer4.pdf
rst2pdf 05-layer7.rst pdfs/05-layer7.pdf
