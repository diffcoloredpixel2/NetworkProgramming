import socket, struct
'''
6) Write a Python client that packs the following and sends to my server listening on IPv6 and TCP port 9001. : 

- The number I assigned you (i.e. the last value of your A:C:7:9::X address) as a unsigned long
- -5 (negative 5) as a signed short
- 192.168.1.242 as a 32 bit unsigned integer
- your last name as a string

You will recieive a string back indicating success/failure
'''

data = struct.pack('!LhI20s', 1337, -5, 1921681242, b'ortega')
server_address = ('a:c:7:9::242', 9001)

s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
s.bind(('', 1337))
s.connect(server_address)

s.send(data)

data = s.recv(1024)
print(data)
