import SocketServer, socket, struct
'''
6) Write a Python client that packs the following and sends to my server listening on IPv6 and TCP port 9001. : 

- The number I assigned you (i.e. the last value of your A:C:7:9::X address) as a unsigned long
- -5 (negative 5) as a signed short
- 192.168.1.242 as a 32 bit unsigned integer
- your last name as a string

You will recieive a string back indicating success/failure
'''
assigned_addresses = {
    20   : 'matt',
    21   : 'phil',
    22   : 'brandt',
    23   : 'armyGuy',
    24   : 'skin',
    25   : 'brit',
    1337 : 'ortega'
}

ans_key = {
    'assigned_address'  : assigned_addresses,
    'signed_short'      : -5,
    'ipaddr'            : 1921681242,
}

srv_config = {
    'listen_host' : '::1',
    'listen_port' : 9001,
    'accept_port' : [ 1337 ]
}

srv_msg = {
    'pass'          : '1337 though art.',
    'fail'          : 'Not so 1337 today are we.',
    'bad_port'      : 'This is not the port you\'re looking for',
    'not_your_num'  : 'No one should have this number assigned?',
    'bad_shorts'    : 'Are you sending {} as a signed short?'.format(ans_key['signed_short']),
    'bad_ipaddr'    : 'Are you sending {} as a 32 bit unsigned int?'.format(ans_key['ipaddr']),
    'bad_names'     : 'Are you sending your last name?',
    'bad_match'     : 'Are you sure you\'re sending the correct assigned number?',
    'dunno'         : 'Dunno what happened here, you did something \'exceptionally\' bad!'
}


def respond(buf):
    try:
        data = list(struct.unpack('!LhI20s', buf))
        data[3] = data[3].strip('\x00')
        print("ATTEMPT: ", data)

        if data[0] not in assigned_addresses:
            return srv_msg.get('not_your_num')

        if data[1] != ans_key['signed_short']:
            return srv_msg.get('bad_shorts')

        if data[2] != ans_key['ipaddr']:
            return srv_msg.get('bad_ipaddr')

        if data[3].lower() not in assigned_addresses.values():
            return srv_msg.get('bad_names')

        if assigned_addresses[data[0]] != data[3].lower():
            return srv_msg.get('bad_match')

        ret = srv_msg.get('pass')
        data[3] = data[3].strip('\x00')
        print("SUCCESS: ", data)
        print("")

    except Exception:
        ret = srv_msg.get('dunno')

    return ret


class TestReqHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        print('Connection from: {0}'.format(self.client_address))
        if self.client_address[1] not in srv_config.get('accept_port'):
            self.request.sendall(srv_msg.get('bad_port'))
        else:
            self.request.sendall(respond(self.request.recv(1024)))


class ThreadSrv(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    address_family = socket.AF_INET6


if __name__ == '__main__':
    server = ThreadSrv((srv_config.get('listen_host'), srv_config.get('listen_port')), TestReqHandler)
    try:
        server.serve_forever()
    finally:
        server.shutdown()
