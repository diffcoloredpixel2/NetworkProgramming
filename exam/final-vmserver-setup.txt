
June 2017

ens33     Link encap:Ethernet  HWaddr 00:00:0b:ad:c0:de  
          inet addr:192.168.0.55  Bcast:192.168.0.255  Mask:255.255.255.0
          UP BROADCAST MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

ens38     Link encap:Ethernet  HWaddr 00:00:13:37:f0:0d  
          inet6 addr: a:c:7:9::242/64 Scope:Global
          UP BROADCAST MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

/index3.html
/images/covfefe.jpg


/etc/radvd.conf 
interface ens38
{
   AdvSendAdvert on;
   prefix a:c:7:9::/64
   {
        AdvOnLink on;
        AdvAutonomous on;
   };
   prefix 1337:4141:4141/64
   {
        AdvOnLink on;
        AdvAutonomous on;
   };
   prefix 1:22:333:4444::/64
   {
        AdvOnLink on;
        AdvAutonomous on;
   };
};
