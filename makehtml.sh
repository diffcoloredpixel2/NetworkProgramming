#!/bin/bash

mkdir htmls/
rst2html 00-concepts.rst htmls/00-concepts.html
rst2html 01-bsdsockets.rst htmls/01-bsdsockets.html
rst2html 02-layer2.rst htmls/02-layer2.html
rst2html 03-layer3.rst htmls/03-layer3.html
rst2html 04-layer4.rst htmls/04-layer4.html
rst2html 05-layer7.rst htmls/05-layer7.html
