:title: Introduction to Network Programming
:data-transition-duration: 1500
:css: networking.css

====================================
Layer 4 - Transport Layer Protocols
====================================

Layer 4 handles connections between two IP addresses. 

There are two types, UDP and TCP

#######################

====================================
Layer 4 Overview  
====================================

- Introduction to TCP

- TCP Flags

- Sequence and Acknowledgements

- TCP Handshake

- TCP Teardown

- TCP State

- Traffic Control

- TCP Header

- TCP Fields

- Introduction to UDP

- Protocols using UDP

- UDP Header

- UDP Fields

- Labs

#######################

====================================
TCP
====================================

TCP is a connection oriented protocol that provides error checking and reliability of communication

Most protocols commonly used on the internet use TCP, including HTTP, SMTP, and SSH

#######################

====================================
Intro to TCP
====================================

TCP connections open with a handshake, data is transmitted, and then the connection is closed with a teardown

TCP provides reliable transfer via:
  - Correctly ordering packets recieved in arbitrary order
  - Validating received packets were not corrupt
  - Re-requesting packets that were corrupt or not recieved at all 
  - Flow and congestion control 
  - Requires positive acknowledgement before next data transmission

#######################

====================================
TCP FLAGS
====================================

TCP uses a bit field to represent flags.

NS - Nonse Sum, used to counter an old method for an attacker attempting to "hide" traffic

CWR - Congestion Window Reduced, an acknolwedgement of congestion notification (ECE)

ECE - Explicit Congestion Notification, notifies sender of network congestion, or that the remote side is ENC capable (if sent along with SYN)

URG - Urgent field is valid in this transmission

ACK - Acknowledgement field is valid. Is on almost all tranmssions except for initial SYN

PSH - Requests data be pushed to application 

RST - Resets the connection

SYN - Requests a Sequence No sync

FIN - No more data to send

#######################


====================================
PSH vs URG
====================================

PSH - Force the reciever to push all data to the process immediately. Used for interactive things where keystrokes should be processed relatively quickly by Layer 7. Netcat does this to force writing to the terminal. Telnet is another example

URG - Not well documented or implemented. Urgent pointer indicates how much data is urgent, from the beginning of the segment.  Only the urgent data should be provided to the process in an out-of-band manner. 

Neither has a programmer API to actually set the value. 


#######################

====================================
Sequence and Acknowledgements
====================================

Sequence numbers indicate the first byte of data currently being sent. (Typically the last recieved ACK)

Acknowledgement numbers indicate two things, what data was received and the next byte expected 
  - Normally it is Sequence Number + Data Len + 1
  - If no data has been transmitted, use previous Sequence Number (still expecting the same next byte)

During the initial handshake:
  - Host A randomly generates a sequence number (The acknowledgement number field is just filled in with junk)
  - Host B will acknowledge A's seq number, and generate a random seq no of it's own
  - Host A will acknowlege B's seq number and the connection is established


#######################

====================================
Sequence and Acknowledgements
====================================

With NO DATA transmitted (e.g. keep alive packet, or a one-sided series of transactions):
  - A sender that is simply ACKing and not sending data of it's own, will re-use the same Sequence Number
  - Sequence Numbers may be re-acked until other side actually sends some data

During normal data transmission
  - Sequence numbers mark the beginning of most recent data sent. It starts at the random number chosen during the handshake and is cumulative
  - Acknowledgement numbers = seq number + data recv'd since last ACK + 1

Errors will result in the Acknolwedgement number being set to the last successfully recieved CONTIGUOUS block of data + 1
    - A sends 10 data, with a seq of 1, but B only recieves 1-7, the Ack number will be 8
    - A sends 10 data, with a seq of 1, but B only recieves 1 and 4-10, the Ack number will be 2

#######################

====================================
Selective Acknowledgements (SACK)
====================================

Originally, the data after the error (either 8-10 or 4-10 in the examples above) would be dropped. 

RFCs 1017 and 2018 introduced/refined selective acknowledgements. It allows all segements recv'd properly to be ACKed, resulting in only the missing/wrong data being resent

To facilitate this, a SACK TCP Option header must be created and appended to the TCP header. Negotiation for the use SACK is done at the beginning of the TCP connection 

It uses a "left edge" and "right edge" to pinpoint the missing data

The ACKno will be the same as a non-SACK connection. The appended header will also include the SACK option for TCP indicating the other data it recieved. The sender may then retransmit the missing data

#######################


====================================
TCP Handshake
====================================

.. image:: img/tcp_handshake.png

#######################


====================================
TCP Teardown
====================================

.. image:: img/tcp_teardown.png

#######################



====================================
TCP state
====================================

TCP sockets have numerous states. Many of these can be seen in the output of a properly (or lucky) timed netstat command

Handshake states
  - LISTEN: Awaiting a SYN
  - SYN-Sent: A client has sent the SYN, and is awaiting the SYNACK
  - SYN-RECIEVED: A server has recieved and a SYN, sent a SYNACK, and is awaiting the ACK

Data states:
  - ESTABLISHED: Normal data transmission occuring
  - FIN-WAIT-1: Client FIN sent, awaiting acknowledgement from Server. May still recieve data
  - FIN-WAIT-2: Client FIN acknolwedged by Server (half closed), awaiting Server FIN. May still recieve data 

  - CLOSE_WAIT: Client FIN recieved and acknowleged by Server. Waiting for Server to send it's own FIN (This is the server view of FIN-WAIT-1 & 2)
  - LAST-ACK: Server sends FIN. Awaiting Client to acknowledge it for teardown
  - TIME-WAIT: There is a "cooldown" period before the socket can be closed for good 
  - CLOSED: No more connection



TIME-WAIT is why you occasionally get errors that a port is in use when rapidly re-running code that creates network socket. 
  - You can use socket options to solve this


#######################


====================================
RST and ICMP
====================================

Both RSTs and FINs (eventually) result in a teardown, but the previous state listing did not reference RST at all. 

A RST can be thought of as aborting the connection (i.e. ESTABLISHED directly to TIME-WAIT/CLOSED). 

For those with Linux experience, a good a analogy is how 'kill -9' terminates a process regardless of what the process is currently doing

In TCP, a RST usually takes the place of an ICMP Port Unreachable message for closed ports

#######################


====================================
Traffic Control
====================================

Window scaling is determined during the handshake and is determines the maximum data to be received before being ACKed. 

Flow control in TCP is accomplished using a sliding window. Receievers specify the amount of data that they are willing to recieve, and the sender will only send that much before waiting for an ACK. If the window is 0, transmission stops for a timeout to allow the reciever to ACK with a new window. 

Congestion is controlled by the TCP stack using timers and various algorithms to estimate round trip time, and will speed up or slow down data based upon that information. 


In depth analysis of these are out of the scope of this class


#######################

====================================
TCP Header
====================================

.. image:: img/tcp_header.png

#######################

====================================
TCP Fields
====================================

Source Port – sending port

Destination Port – Receiving port

Sequence Number – Initially random. Each new transmission adds the size of the data

Acknowledgment Number – The next byte expected to be received.

#######################

====================================
TCP Fields
====================================

Data offset - Size of TCP header in 32bit words

Reserved – 0's

Flags – Bit mask of all TCP flags

Window size – Max number of bytes reciever can handle


#######################

====================================
TCP Fields
====================================

Checksum – Checksum of header and data

Urgent Pointer – Only valid if URG flag set

Options – Allows for expanded uses

#######################

====================================
UDP
====================================

UDP is connectionless and best effort delivery. Error checking is limited to a UDP checksum.

Being connectionless minimizes overhead

Removing error checking, reordering, and other nice functionality from TCP increases speed since retransmissions do not occur and there is no need to wait for acknowledgements.

#######################

====================================
Protocols using UDP
====================================

UDP is particularly well suited to protocols where a single message is sent the repsonses are also single messages:
  - DNS, DHCP, SNMP, RIP

Protocols and applications designed to handle a high volume of traffic can often internally handle some packet loss:
  - VOIP, Streaming Media, Video Games


#######################

====================================
UDP Header
====================================

.. image:: img/udp_header.png

#######################

====================================
UDP Fields
====================================
Source Port – sending port

Destination Port – Receiving port

Length – Size in bytes of header + data

Checksum – Error checking

#######################


====================================
Labs - Special Note
====================================

Lab 4C is probably the most difficult one of the class. I fully expect this to take a significant amount of time. 

There is no need to rush. 

#######################

====================================
Lab 4A - IPv6 Multicasting
====================================

We will be doing link-local scope multicasting using traditional UDP sockets (not RAW). 

Use the following multicast address FF02::A:C:7:9

Create a terminal based chat program that uses UDP and IPv6 Multicast. By terminal based, I mean printing strings to the terminal window. Make 2 python files, one for printing all recieved chats, and another just for sending your messages. You will need to run each in a seperate terminal window. 

Message strings should follow the following format
  
  Username;Text Goes Here

The reciever should display recieved chats in the following format

  Username (IPv6 Addr): Text Goes Here

HINTS:

Ref: http://man7.org/linux/man-pages/man7/ipv6.7.html

Using the reference set the following socket options, with a level of socket.IPPROTO_IPV6 

Sender
    - Set the multicast hops to 5

Receiver
    - Set the socket's multicast group (This applies for multicasts NOT to ff02::1)
    - The group is a value obtained by combining the following:

      - Packing the multicast IPv6 addr using socket.inet_pton()
      - Packing a 32 bit unsigned integer with the value 0, using struct.pack()

::

      dst_ip = "FF02::a:c:7:9"
      group_bin = socket.inet_pton(socket.AF_INET6, dst_ip)
      group = group_bin + struct.pack('@I', 0)
      # use group as the VALUE for the function call to setsockopt()

#######################


====================================
Lab 4B
====================================

Fill out the TCP worksheet. There are 4 tabs representing 4 questions in spreadsheet form. Use the provided information to fill in the blanks


#######################

====================================
Lab 4C - TCP Reset Lab
====================================

Note: This one must use RAW sockets. You must also run one netcat on Linux VM, and the other on your Windows host. 

Open wireshark and sniff the ens33 interface

    - Filter on port 8888

Set up a netcat listener and connector on port 8888 using IPv6

    - DON'T TYPE ANYTHING

Observe the latest packet in wireshark (TCP Handshake initially, data afterwards)

Using raw sockets, forge an ACK/RST packet from the server that kills the connector

    - You will know it worked when the connector returns to the terminal prompt. The listener will likely still be running

    - If both netcats are still running, type something on one end and verify it on the other side. Then try again using the latest packet in wireshark

Hints
    - You MUST have checksum. Use send_chksum() from the helper code
    - Seq nos and Ack nos are important!



UPON SUCCESS: Repeat and kill the the server instead of the connector


#######################

====================================
Lab 4 BONUS 1
====================================

The TCP reset lab is somewhat contrived, so we will make it more realistic. To actually do this in the real world, you will have a short amount of time to identify traffic, calculate the proper Ack and Seq Nos, and fire your traffic.

Repeat the TCP reset lab, except type and send 5 messages before attempting to kill it. At least 1 message must be sent from the listener and at least 1 message must be sent from the connector. The remaining messages may be from either side or mixed. Order of the messages is irrelevent

The contents of the message should be a few words each

UPON SUCCESS: Repeat and kill the the server instead of the connector


#######################


====================================
Lab 4 BONUS 2
====================================

Based on the TCP reset lab but instead of killing the connection, attempt to inject a message that is printed to the terminal 

UPON SUCCESS: Repeat and inject the the server instead of the connector


#######################


====================================
Layer 4 Overview  
====================================

TCP is connection oriented protocol with error handling and traffic control

TCP begins with a handshake and ends with a teardown

TCP Flags and Sequence/Acknowledgement numbers are key components

TCP's functionality adds overhead to all traffic

UDP is connectionless and best effory delivery, with no error handling

UDP is designed for minimal overhead and single message/response traffic 

#######################

====================================
Backup labs if needed
====================================

Ignore these

#######################

====================================
TCP Flag values Lab
====================================

Given a TCP flag field value of X, what flags are set? What does that indicate about the packet?


#######################


====================================
Extend Lab 4A
====================================

Modify the 4A lab to include direct messaging, without breaking existing functionality

A message that begins with @username should then be sent directly to that user


#######################



====================================
Extend Lab 4D - TCP Reset Lab v2
====================================

Repeat the TCP reset lab, except type and send 5 messages before attempting to kill it. At least 1 message must be sent from the listener. At least 1 message must be sent from the connector. The remaining messages may be from either side or mixed.

Order of the messages is irrelevent

The contents of the message should be a few words each

UPON SUCCESS: Repeat and kill the the server instead of the connector
