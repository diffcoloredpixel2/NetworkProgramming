:title: Introduction to Network Programming
:data-transition-duration: 1500
:css: networking.css

====================================
Layer 3
====================================

Basics of Layer 3 and IP

#######################

====================================
Layer 3 Overview
====================================

- Introduction to IP

- IPv4 Addresses

- IPv4 Address Classes

- Subnet Masks

- CIDR

- Routers

- Routing Protocols

- Routing Basics

- Routing

- IPv4

- IPv4 Header Fields

- NAT and PAT

- IPv6

- IPv6 Addresses

- IPv6 Address Types

- IPv6 Global Unicast

- IPv6 Multicast

- IPv6 Anycast

- IPv6 Header Fields

- ICMP

- ICMP Types and Codes

- Ping

- Reasons for ICMP Error Messages

- MTU and Fragmentation

- ICMP Header

- NDP

- IPV6 Labs

#######################


====================================
Introduction to IP
====================================

Ethernet addressing is limited to networks sharing the same physical medium (i.e the broadcast domain). In order to connect multiple distinct networks, a new technology is needed.

The Internet Protocol (IP) is the Layer 3 protocol that we to address hosts and route traffic across networks.

There are two main types, IPv4 and IPv6

#######################


====================================
IPv4 Addresses
====================================
IPv4 Addresses are represented in “dotted decimal” notation

IPv4 addresses are 32 bits, so each number is represented by a single byte

    You can represent an IP using an unsigned 32 bit data type, e.g. uint32_t in C

There are 2^8 possible values for an octet ranging from 0-255

#######################

====================================
IPv4 Address Classes
====================================

Class A - first bit is 0 (First octet 1-127)

Class B - first TWO bits are 10 (First octet is 128-191)

Class C - first THREE bits are 110 (First octet is 192-223)

Class D - first FOUR are 1110 (First octet 224-239) Used only for multicasting and thus no network/host specified

Class E - The following IPs 240.0.0.0 to 255.255.255.254. Used for experimentation only. No network/host specified

#######################

====================================
Subnet masks
====================================

Subnet masks were introduced to identify the NETWORK and HOST bits of an IP

Subnet masks are a special kind of IPv4 address used in tandem with a normal IPv4 addresses to provide that context

They are used when configuring interfaces of network devices (PC's, Routers, etc). They are NOT transmitted with regular network traffic.

#######################

====================================
Subnet masks
====================================

Subnet masks are a contiguous block of 1's followed by any remaining bits set to contiguous 0's. The contiguous bits may span several octets.

Mixing 1's and 0's together is not allowed. This means that the only valid numbers in a subnet mask are: (0, 128, 192, 224, 240, 248, 252, 254, 255)

::

  255           .255          .248          .0
  11111111      .11111111     .11111000     .0


#######################

====================================
Subnet masks
====================================

All bits in the IP address that are masked with 1's determine the NETWORK. This may be called the "network prefix"

All 0's are determine the HOST bits

If you bitwise AND an IP and a subnet mask, you will get the network IP.

::

  172.16.237.18
  255.255.248.0

  10101100      .00010000     .11101 | 101     .00010010
  11111111      .11111111     .11111 | 000     .00000000

  10101100      .00010000     .11101 | 000     .00000000

  172.16.232.0 is the network IP


#######################


====================================
CIDR
====================================

CIDR notation is a quick way of writing a subnet mask that allows for advanced networking

After the IP address put a slash followed by the number of network bits

::
  192.168.1.2 with a mask 255.255.224.0

  11111111      .11111111     .11100000     .00000000

  There are 19 contiguous 1's, so the CIDR notation is 192.168.1.2/19

#######################


====================================
Subnetting in English
====================================

All bits covered by the network portion may not change, doing so implies a new network.

If we borrow bits from the host section and make them part of the network section (by adding to the subnet mask) we can create logical groupings of hosts, or subnets.


Example

We have 192.168.1.2/19, which tells is the first 19 bits must stay the same

We can borrow three more bits from the host making the new network 192.168.1.2/22

Those three bits allow for 8 different subnets (2^3 = 8) at the expense of some host IDs

#######################

====================================
Routers
====================================

Routers operate on Layer 3 address (IP)

They ignore Layer 2 addresses for decision making

Multiple Collision Domains

Multiple Broadcast Domains

#######################


====================================
Routing Protocols
====================================

Routing protocols allow neighboring routers to collaborate dynamically

There are two main types, distance-vector and link-state

Distance-vector protocols attempt to calculate the "distance" between networks. Usually this is the total number of hops, or the sum of all weights on a path.

Link-state protocols are more concerned with speed and current state of the connections. A longer path with a faster travel time will be prioritized over a path with less hops.

Hybrid protocols also exist

#######################

====================================
Routing Protocols
====================================

Each routing protocol defines the metric(s) used to calculate weights.

Weights are calculated individually on each router for each known network. The weight for the exact same network may be different on different routers.

Static routes may also be set, with arbitrary weights, by a network administrator

The “best” weight is used by a router to decide where send the packet. Weights are usually represented as an integer, and the lowest number is considered best.


#######################

====================================
Routing Protocols
====================================

Routers have routing tables that map a NETWORK, WEIGHT, and the NEXT HOP ADDRESS

Routing tables are populated by the information exchanges dictated by the specific routing protocol being used on that router and it's neighbors

      - Neighbors advertise what NETWORKS they know about, and their WEIGHTS

      - The NEXT HOP will either be the IP of the router that advertised the best path to a destination, OR it will be the locally connected network

#######################

====================================
Routing Basics
====================================

Packets are routed independently of each other, even if they are to the same destination

Traffic destined for a private IP address will not be routed onto the public internet without special configurations

Broadcast traffic will not be routed outside of that network

#######################

====================================
Routing
====================================

When a router receives a packet, the destination IP of a packet is compared to the networks in the routing table

  - If the network is directly connected, send the packet to that network

  - If the network is known, but not connected, send it to the NEXT HOP specified in the routing table with the 'best' weight

  - Otherwise send it to the DEFAULT GATEWAY

######################

====================================
What about layer 2?
====================================

Layer 2 is always limited to a local LAN

Each interface on a router is effectively on a new LAN

Therefore, after a decision is made the layer 2 source and destination are updated to be the current router MAC and next hop MAC respectively.

The Layer 3 addresses DO NOT CHANGE during routing.

#######################

====================================
IPv4
====================================

IPv4 was adopted by IETF in 1981. IPv4 is the common version of IP we are used to dealing with

Initially, every device was to have it's own IPv4 address.

The explosion of the Internet dramatically increased the number of connected devices so IPv4 was modified slightly to curb address exhaustion.

These modifications included NAT, PAT, and subnetting

The supply of IPv4 has been exhausted (most recently North America in Sept 2015)

#######################

====================================
IPv4 Header
====================================

.. image:: img/ipv4_header.png

#######################


====================================
IPv4 Fields
====================================

Version - Set to 4 for IPv4

Header Length – Size of IP header

Differentiated Services Code Point (DSCP) – Formerly “Type of Service (TOS)”, it was redefined by RFC 2474. Used for new technologies that require real time data streamed over the network. We will not deal with this in this class

Explicit Congestion Notification – Formerly part of TOS, used to indicate network congestion. NOTE: This is not the same as TCP's congestion handling


#######################


====================================
IPv4 Fields
====================================

Total Length – Size of packet in bytes, including header and data

Identification – Identifies a group of IP fragments

Flags – Fragment flags

::

  Bit Indicator 	RFC 791 Definition
  0xx 	          Reserved
  x0x 	          May Fragment
  x1x 	          Do Not Fragment
  xx0 	          Last Fragment
  xx1 	          More Fragments

  from: http://www.wildpackets.com/resources/compendium/tcp_ip/ip_fragmentation

Fragment offset – offset of fragment from original packet

#######################


====================================
IPv4 Fields
====================================

Time to Live – Decremented first thing at each hop, packet is discarded when TTL is 0

Protocol – Protocol used in data portion

Header checksum – Error Checking

Source / Destination IP – Do not change during routing

#######################


====================================
IPv4 Fields
====================================

IPv4 has the ability for optional headers, but they are typically not used in the wild.

Seeing them in IPv4 traffic is worth an investigation if you are analyst/admin

#######################

====================================
NAT and PAT
====================================

NAT - Network Address Translation

  - Modifies an address inside your network, to a global IP (may be single address or from a pool)

  - NAT does not require private addresses


PAT - Port Address Translation

  - A mapping of inside IP/port to public IP/port is maintained by router

  - Return traffic will have the same Public IP/port which can be looked up and forwarded to the appropriate inside IP/port

#######################

====================================
Break
====================================

Quick Break


#######################

====================================
IPv6
====================================

IPv6 is the latest version. It returns to the cleaner design of IP that IPv4 had before attempts to curb exhaustion happened.

Differences from IPv4

  - Back to single address per host, address translation is no longer needed
  - Multicasting, QoS, IPSec, and Encryption is built in
  - IPv6 can autoconfigure itself in a local network


See referenced for IPv6 RFC

#######################

====================================
IPv6 Addresses
====================================

IPv6 addresses are 128 bits long and represented by groups of 1-4 hex characters seperated by a : (aka a "hextet", or more formally "hexadectet")
  - 2001:0db8:0000:0000:0000:ff00:0042:8329

Using the recommendations in RFC5952 section 4.2 we can shorten that IPv6 address.

Leading 0's in a grouping may be omitted

  - 2001:db8:0:0:0:ff00:42:8329

It may contain at MOST a single :: which represents the largest contiguous block (longer than than 16 bits) of 0's in the address

  - 2001:db8::ff00:42:8329 last

If multiple contiguous blocks are the same length, shorten the left most one

  - 2001:0:0:AAAA:0:0:B:CC becomes 2001::AAAA:0:0:B:CC

If the contiguous 0's only encompass a single hextet, do NOT replace it with a \::

  - The 2nd hextet in A:0:BBBB:CCC:D0:22:FF:4412 should never be reduced with a \::



#######################

====================================
IPv6 Address Types
====================================

Generally there are three types of addresses
  - Unicast
  - Multicast
  - Anycast

There are also several scopes. From smallest to largest:
  - Node Local (aka Interface Local) is local to your machine (e.g. loopback)
  - Link Local is everything on the local LAN (e.g. reachable via layer 2 broadcast)
  - Subnet Local may be used for subnets can span multiple physical links
  - Admin Local is the smallest group that cannot be auto dervived based on physical connection or multicast configuration
  - Site local is for internetworks (multiple LANs) at specific site 
  - Organizational local is for Internetworks (multiple LANs) at multiple sites
  - Global is anywhere on the internet

  Each subsequent scope is logically bigger than the previous one. Scopes may be ignored if they do not apply to the network in question.

#######################

====================================
IPv6 Address Types
====================================

For this class, we will be focusing on

  - Global ("Public" in IPv4)
  - Site Local ("Private" in IPv4)
  - Link Local (Everything accessible via the link-layer/layer 2/MAC/Ethernet)
  - Node local (loopback)

There is no IPv6 broadcast address. Multicast addresses fulfill that role

#######################

====================================
IPv6 Address Types
====================================

The type of address is specified by the network portion of the address as specified in CIDR notation

Any host bits may be manipulated freely to create new addresses.

+---------------------+-------------------------------------+--------------------------------+
| IPv6                | Description                         | IPv4 Equivilent                |
+=====================+=====================================+================================+
| ::/128              | Unspecified/Default                 | 0.0.0.0                        |
+---------------------+-------------------------------------+--------------------------------+
| ::1/128             | Loopback                            | 127.0.0.1                      |
+---------------------+-------------------------------------+--------------------------------+
| fec0::/7            | Site local, not routed              | 10.0.0.0/8, 192.168.0.0/16     |
+---------------------+-------------------------------------+--------------------------------+
| fe80::/10           | Link Local, not routed              | 169.254.0.0/16                 |
+---------------------+-------------------------------------+--------------------------------+
| ff00::/8            | Multicast                           | Class D                        |
+---------------------+-------------------------------------+--------------------------------+
| 2001::/32           | Teredo, Allows IPv6 over IPv4       | n/a                            |
+---------------------+-------------------------------------+--------------------------------+
| 2002::/16           | 6to4, Allows IPv6 over IPv4         | n/a                            |
+---------------------+-------------------------------------+--------------------------------+

NOTE: This list is not exhaustive

http://www.iana.org/assignments/ipv6-multicast-addresses/ipv6-multicast-addresses.xhtml

#######################

====================================
IPv6 Global Unicast
====================================

A global unicast address can be broken down with the following rules.

  1) A global routing prefix must be at least 48 bits. 
  2) Global routing prefix + subnet id = first 64 bits
  3) Remaining 64 bits are the host id

 The first 64 bits are the routing prefix

.. image:: img/ipv6_global_unicast.png

from: https://mrncciew.com/2013/04/05/ipv6-basics/

#######################

====================================
IPv6 Multicast
====================================

If you need to use a multicast IPv6 address, be sure to ALSO use the Layer 2 (Ethernet) multicast address (33:33:00:00:00:01)

A global multicast address can be broken down as follows:

.. image:: img/ipv6_multicast.png

From: https://mrncciew.com/2013/04/05/ipv6-basics/

From: http://www.txv6tf.org/wp-content/uploads/2013/07/Martin-IPv6-Multicast-TM-v3.pdf

#######################


====================================
IPv6 Multicast
====================================

+---------------+-----+-------------------------------------+----------------------------------------+
| IPv6 Multicast Address       | Description                                                         |
+===============+=====+=====================================+========================================+
| FF01:0:0:0:0:0:0:1           |  All Nodes (Node Local)                                             |
+---------------+-----+-------------------------------------+----------------------------------------+
| FF02:0:0:0:0:0:0:1           |  All Nodes (Link Local)                                             |
+---------------+-----+-------------------------------------+----------------------------------------+
| FF01:0:0:0:0:0:0:2           |  All Routers (Node Local)                                           |
+---------------+-----+-------------------------------------+----------------------------------------+
| FF02:0:0:0:0:0:0:2           |  All Routers (Link Local)                                           |
+---------------+-----+-------------------------------------+----------------------------------------+
| FF05:0:0:0:0:0:0:2           |  All Routers (LL OSPFv3)                                            |
+---------------+-----+-------------------------------------+----------------------------------------+

NOTE: This link is not exhaustive

#######################

====================================
IPv6 Anycast
====================================

Anycast addressing does not operate like traditional addressing

Anycast addresses are in the global unicast range. The same global unicast address is assigned to multiple interfaces (perhaps on several different nodes)

Traffic destined to the anycast address will be delivered to the "nearest" host with that address. It's a trick to rely on a routing protocol which determines the "closest" host that was configured with the address

Don't use an anycast address as a source address

This seems strange. Why do it?

Some services are run on multiple servers (DNS or NTP for example). You would assign anycast addresses where multiple severs provide the same service but only one reply is required to use the service


#######################

====================================
IPv6 Header
====================================

.. image:: img/ipv6_header.png

#######################


====================================
IPv6 Fields
====================================

Version – Set 6 for IPv6

Traffic Class – Categorizes traffic for QoS purposes. ECN is last 3 bits

Flow Label – Request special handling by routers. May not be honored

Payload Length – Size of payload in OCTETS. Including any extension headers

#######################


====================================
IPv6 Fields
====================================

Next Header – Layer 4 header of payload. Uses same values as IPv4's protocol field

Hop Limit – Same as TTL

Source / Destination - Do not change during routing

#######################


====================================
IPv6 Headers
====================================

IPv6 Headers are identified by number. They are not required to be used, however they provide useful features that were absent or poorly implemented in IPv4.

For security we have the following. Be aware that encryption at higher layers is still adviseable.

51 = Authentication Header (AH) 
  - Contains information used to verify the authenticity of most parts of the packet.

50 = Encapsulating Security Payload (ESP) 
  - Carries encrypted data for secure communication.

Other options include: 

0 = Hop-by-Hop Options 
  - Options that need to be examined by all devices on the path.

60 = Destination Options (before routing header) 
  - Options that need to be examined only by the destination of the packet.

43 = Routing 
  - Methods to specify the route for a datagram (used with Mobile IPv6).

44 = Fragment 
  - Contains parameters for fragmentation of datagrams.



135 = Mobility (currently without upper-layer header) 
  - Parameters used with Mobile IPv6.

From wikipedia

#######################


====================================
Break
====================================



#######################

====================================
ICMP
====================================

Internet Control Message Protocol

A "helper" protocol that supports IP, and provides information and error reporting 

Many messages have been deprecated, reserved, or are obsolete

Each message has a Type, and each type may have several Codes

#######################


====================================
ICMP Types and Codes
====================================

See References page for bigger list

Type 0 Echo Reply
  - Ping

Type 3 Destination Unreachable
  - Code 0 Destination Network Unreachable
  - Code 1 Destination Host Unreachable
  - Code 3 Destination Port Unreachable (NOTE: See Layer 4 for slight modification regarding TCP)

Type 8 - Echo Request
  - Ping

Type 11 Time Exceeded
  - Code 0 TTL Exceeded
  - Code 1 Fragement Reassembly Time Exceeded

Type 30 Traceroute
  - Deprecated

#######################


====================================
Ping
====================================

Ping is ubiquitous. It shows the connectivity status between two hosts.

It is an example of ICMP used for information purposes

Host A initiates request to Host B
  - ICMP Type 8

Host B receives it, and sends a response to A
  - ICMP Type 0


#######################


====================================
ICMP error messages
====================================

If the packet cannot be delivered to a host, an ICMP message is generated and returned to the sender. 

The ICMP Type and Code specifies the error

Part of the original packet is also included to help the sender know which transmission caused the error

#######################


====================================
Reasons for ICMP error messages
====================================

Below, I've captured some anecdotal reasons you will see some common ICMP errors. They are NOT absolutes, but just things to think about should you encounter them.

  - Network unavailable typically means there is no route to the network

  - Host unreachable is usually one of two reasons

    - Layer 1 is broken, host physically offline
    - A security feature is preventing access to the host (firewall, proxy, etc)

  - Port Unreachable is usually one of two reasons

    - A service is likely not running on that port*

    - A security feature is preventing access to that port (firewall, proxy, etc)

  - TTL Exceeded

    - Something is unusual about the route to the host that makes the path quite long

  - Reassembly Time Exceeded

    - I have never encountered this, however a severely degraded connection with multiple varying MTUs could theoretically cause this

#######################


====================================
MTU and Fragmentation
====================================

Maximum Transmission Unit is the maximum size of the payload on a given link-layer (Layer 2) protocol. (1500 bytes for Ethernet)

The max size of an IP packet is 65535 bytes (unless jumbogram extension header is used)

IP packets that are bigger than the link-layer protocol MTU are FRAGMENTED into several frames, and the recipient is responsible for reassembly

MTU can vary at each hop. This means it is possible for every node on the path to the destination to further fragment
  - Senders usually try to discover the smallest MTU on the path and shrink their own transmissions appropriately

Packets larger than MTU are dropped and ICMP message returned to sender indicating the packet is too big



#######################

====================================
NDP (Part of ICMPv6)
====================================

Neighbor Discovery Protocol is a collection ICMPv6 messages for auto configuration, host discovery, and general awareness. 

These ICMPv6 messages are defined in RFC4861, section 4. Because they are ICMP, each header always begins with Type, Code, Checksum then branches off into fields specific to that message.

Each specific NDP header also has several "optional" headers that may be appended (see section 4.6 in RFC 4861). They are considered optional because the provide specific functionality that is not always required. However, my labs will likely require them to accomplish specific objectives

#######################

====================================
NDP Messages (Part of ICMPv6)
====================================

Neighbor Solicitations - Map a known IPv6 address to a layer 2 address

Neighbor Advertisements - Sent in response to a Neighbor Solicitation or you can to broadcast your own layer 2 address to the network unsolicted

Router Solicitation - Request a Router Advertisement

Router Advertisements - Sent in response to a Router Solicitation. It basically says here are all networks I know about

  - An RA also tells you if the sender is the first hop for that network, or if the network is actually considered "on-link" (i.e. directly connected because multiple prefixes on the same LAN)

Redirect - If you see traffic destined somewhere, and you know a better route to a given host you can send this to have the sender redirect their traffic


#######################

====================================
Helper Code v2
====================================

Note the changes to IPv6 and we are now using send_checksum()

::

  from raw_socket_helper import RawSocket_IPv6

  ... Your Code Here ...

  interfaceName = "ens33"   # from ifconfig
  
  raw_socket = RawSocket_IPv6(interfaceName)
  raw_socket.send_chksum(frame)          
  
  #######################


====================================
Coding Suggestions
====================================

Look at the example below for a skeleton that allows extension into layer 3 and above. 

::

  from raw_socket_helper import RawSocket_IPv6

  # Constants
  mymac = "\x00\x0c\x29\xc9\x78\x14"
  myip = ...

  # Layer 2 Header
  dst = ...      # Destination MAC
  src = mymac    # Source MAC
  typ = ...  
  etherhdr = dst + src + typ

  # IPv6 Header
  Ver_traffic_flow = '\x60\x00\x00\x00'
  PayloadLength = ...

  ipv6hdr = Ver_traffic_flow + PayloadLength + ....

  # ICMPv6 Header
  Type = ...
  Code = ...

  icmpv6hdr = Type + Code + ...

  
  # Make the complete frame and send it
  frame = etherhdr + ipv6hdr + icmpv6hdr + ...

  interfaceName = "ens33"   # from ifconfig
  raw_socket = RawSocket_IPv6(interfaceName)
  raw_socket.send_chksum(frame)            

#######################



====================================
LAB 3
====================================

You will be implementing the NDP messages using the IPv6 raw socket helper code. 

Some labs may have further headers. The protocol diagram will end with:

::

      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |   Options ...
      +-+-+-+-+-+-+-+-+-+-+-+-

There is a seperate options format section in the RFC. Each option
begins with a Type and Code, followed by specific header data.

#######################



====================================
LAB 3 Hints
====================================

  Read the RFC before starting. Each message spells out certain values to use in IPv6 headers, ICMPv6 headers, and ICMPv6 options.

  By default, hosts will ignore any traffic that is not 100% RFC compliant (Wireshark does not validate RFC compliance)

  The first 32 bits of the IPv6 header (Version, Traffic Class, Flow Label) should be '\x60\x00\x00\x00'

  All Checksums should be all 0's

  All Reserved should be all 0's

  The payload size in IPv6 header needs to be calculated by you

  Use small timer values while testing!

  Think about the scope of your IPv6 addresses, especially when directed to by the RFC

  What is the multicast MAC address for IPv6?

  If you see "Unable to checksum IP Proto 0x0a" verify you are using the IPv6 object in the helpder code (see new helper code above) If you are, you have an improper field width that causes the IPv6 next header field to be parsed wrong. 


#######################

====================================
LAB 3A
====================================

Generate a valid IPv6 Neighbor Solicitation to your classmate, that elicts a neighbor advertisement.

Ensure both the solicitation and advertisement are viewable in Wireshark

Valid means it conforms the RFC, Wireshark confirms it is not malformed/displays no errors, and your request produces a reply

Depending on the setup of an OS, there MIGHT be a timeout on how many Neighbor Advertisements can be sent out in a period of time. If there is nothing else wrong with your code, wait 2-3 mins and try again. Or choose a different target

#######################

====================================
LAB 3B
====================================

Create a valid Neighbor Advertisement that alters the TARGET MAC address on your classmate's system. You will only see the advertisement in Wireshark, there is no network reply.

::

	1) Neighbor pings your IPv6 address. Let it run for 10 successes
		- If you ping a link-local scope address use 'ping6 fe80::aaaa:bbbb:cccc:dddd%ens33'

	2) Neighbor executes 'watch -n 0.1 ip -6 neighbor show'
		- watch -n 0.1 re-runs the command every 0.1 seconds. You can see changes live

	fe80::5469:2c4c:e7a5:4186 dev enp3s0 lladdr f0:de:f1:ad:4b:d6 (REACHABLE/DELAY/STALE)
	a:c:7:9::7 dev enp3s0 lladdr f0:de:f1:ad:4b:d6 (REACHABLE/DELAY/STALE)

	3) You send your advertisement

	4) Check neighbors cache and see if the target MAC address changed

	

	fe80::5469:2c4c:e7a5:4186 dev enp3s0 lladdr de:ad:be:ef:00:00 (REACHABLE/DELAY/STALE)
	a:c:7:9::7 dev enp3s0 lladdr de:ad:be:ef:00:00 (REACHABLE/DELAY/STALE)


Valid means it conforms the RFC, Wireshark confirms it is not malformed/displays no errors and you altered the target mac address in your neighbors cache

Hint: The target IP will be updated with a target MAC (link-layer) address

#######################


====================================
LAB 3C
====================================

Generate a valid IPv6 Router Solicitation, that elicts a router advertisement. There is a router already set up

Ensure both the solicitation and advertisement are viewable in Wireshark

Valid means it conforms the RFC, Wireshark confirms it is not malformed/displays no errors, and your request produces a reply

#######################


====================================
LAB 3D
====================================

Generate a valid IPv6 Router Advertisement to the class server that indicates you are a default router. 

You must also specify a prefix. You may choose your own prefix, however it must NOT be for the a:c:7:9/64 or fe80/10 network

Ensure the advertisement is viewable in Wireshark

Valid means it conforms the RFC, Wireshark confirms it is not malformed/displays no errors, and your request produces a route on the server (will be displayed on screen)

Your RA will put your link-local address on the screen as shown below

::

  default via <your fe80 address here> dev ens38  proto ra  metric 1024  expires 3576sec hoplimit 255 pref medium


#######################



====================================
LAB 3E
====================================

Modify your Router Advertisement to actually advertise the network prefix you chose is on-link

You may choose your own prefix, however it must NOT be for the a:c:7:9/64 or fe80/10 network

Ensure the advertisement is viewable in Wireshark

Valid means it conforms the RFC, Wireshark confirms it is not malformed/displays no errors, and your request produces a route on the server (will be displayed on screen)

Your RA will put your link-local address on the screen as shown below

::
  dd:7:7:70b::/64 dev ens38  proto kernel  metric 256  expires 28sec pref medium

Hint: Flags


====================================
LAB 3 BONUS
====================================

Create a raw socket that can handle ICMP traffic. Do not use the helper code. 

Have your code listen for an IMCP message. Using the command line, generate send ping6 packets to your system and validate you can recieve the response on your initial socket.

Use struct to unpack the values into the ICMPv6 header. Print the name of each field, it's width, and the value

The generate your own Ping in python, and send it. 

Hint: socket.socket() takes an optional third parameter which can be a constant from the socket module. Ping is ICMP or ICMPv6 depending on which address family you are using

Hint 2: Each message is essentialy self-contained much like UDP

#######################

====================================
References
====================================

RFC:

    IPv4 - https://tools.ietf.org/html/rfc791

    IPv6 - https://www.ietf.org//html/rfc2460

    NDP - https://tools.ietf.org/html/rfc4861

    ICMPv6 - https://tools.ietf.org/html/rfc4443

    ICMP - http://www.faqs.org/rfcs/rfc792.html




Values for IPv4 Protocol Field/ IPv6 Next Header Field:
    http://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml

ICMP Messages:

    https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol

IPv6 Address Multicast Addresses

    http://www.iana.org/assignments/ipv6-multicast-addresses/ipv6-multicast-addresses.xhtml

#######################


====================================
Layer 3 Overview
====================================

IPv4

IPv6

CIDR notation

ICMP

Routing

NDP/ICMPv6
