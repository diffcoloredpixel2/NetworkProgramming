
====================================
IPv4 Address Classes
====================================

The class determined which bits identified the NETWORK.  The remaining uniquely identified a HOST in that network

 - Class A was 8 NETWORK  / 24 HOST
 - Class B was 16 NETWORK / 16 HOST
 - Class C was 24 NETWORK / 8 HOST

Each class supports a different number of networks and hosts.

Networks = 2^(NETWORK BITS - CLASS BITS)

HOSTS = 2^(HOST BITS)

- Class A = 2^(8-1) = 128 networks, each with 16,777,216 
- Class B = 2^(16-2) =  16,384 networks, with 65,536 hosts
- Class C = 2^(24-3) =  2,097,152 networks, with 256 hosts

#######################



====================================
Classful to Classless
====================================

Entities were given an IP that matched how many hosts they needed. Most networks had more than 256 hosts, so they were given a class B IP. 

This began to exhaust the supply of class B IP's since those entities needed more than 256 hosts, but were wasting a vast majority of the 65k possible hosts 

So what is the solution?

Ditch the classes and break apart IP ranges into smaller chunks. Assign the smallest chunk possible to each entity based upon their projected needs.


#######################


====================================
Subnetting
====================================

Subnetting is effectively taking a chunk of IP's and breaking them into smaller chunks. (Similar to the IP range assignment used to comabt exhaustion) 

Subnetting gives us flexibility in addressing the systems in our network and advertising known networks, particularly with PRIVATE IPs

Reading a subnet mask on the host will provide clues to the network you are working on

#######################

====================================
Why subnet?
====================================

What if the network is for a business that has only a couple thousand employees across 5 locations?
  - Without subnetting, we have 1 network with 65k possible hosts. 

As long as we borrow from only the HOST bits, we can create SUBNETS

Borrowing from HOST bits also means the total number of possible hosts decreases, and is divided evenly over each newly created subnet

#######################

====================================
How to subnet 1
====================================

Our ISP gave use the following IP: 172.16.22.47, Mask: 255.255.0.0

First, convert each octet of the mask into binary

::

  11111111      .11111111    .0     .0


#######################

====================================
How to subnet 2
====================================

Second, write the IP address octets above the binary mask
  - If an octet of the mask is not 255 or 0, break the corresponding octet of the IP into binary as well

::

  172               .16      .22   .47
  11111111      .11111111    .0     .0

#######################

====================================
How to subnet 3
====================================

Third, draw a vertical line where the mask changes from 1 to 0

::

  --------------------------|-------------
  172               .16     | .22     .47
  11111111      .11111111   |  .0     .0 

#######################

====================================
How to subnet 4
====================================

The NETWORK bits are all numbers in the IP covered by 1's in the mask (to the left of your line)
  - If an octet of the mask is not 255 or 0, then your vertical line will show you which bits are part of the network. Translate only those bits back into decimal. Assume any host bits are 0

::

  --------NETWORK---------|-------------
  172               .16   | .22   .47
  11111111      .11111111 |  .0   .0

There are 16 network bits and the network IP is 172.16.0.0

#######################

====================================
How to subnet 5
====================================

The HOST bits are all bits in the IP that are not NETWORK bits (right of your line)
  - If an octet of the mask is not 255 or 0, then your vertical line will show you which bits are part of the host. Translate only those bits back into decimal. Assume any network bits are 0

::

  --------------------------|---HOST---
  172               .16     | .22   .47
  11111111      .11111111   |  .0   .0

There are 16 host bits

#######################

====================================
How to subnet 6
====================================

Now what?

(2^N)-2 = number of hosts, where N is the number of hosts bits
  - You can estimate the size of the network

(2^16)-2 = 65534 hosts

Why subtract two?
  - The first address is the network address
  - The last address is a IP broadcast address

#######################



====================================
Example of subnetting 1
====================================

We need a total of 5 networks. To do so, we must borrow the appropriate number of bits from the host portion. Remember that each bit we borrow gives 2^N networks

 - Borrow 0 bits gives 2^0=1 network, which is what we already have
 - Borrow 1 bit  gives 2^1=2 networks, not enough
 - Borrow 2 bits gives 2^2=4 networks, still not enough
 - Borrow 3 bits gives 2^3=8 networks, This will work
 - Borrow 4+ bits also will work

To borrow 3 bits, change the first 3 bits of the HOST portion to 1's and recalculate that octet's value 

::

  255           .255         .0           .0
  11111111      .11111111    .00000000    .0

  becomes

  255           .255         .224         .0
  11111111      .11111111    .11100000    .0

#######################

====================================
Example of subnetting 2
====================================

IP: 172.16.150.47, Mask: 255.255.224.0

First, convert each octet of the mask into binary

::

  172           .16          .22          .47
  11111111      .11111111    .11100000    .0

#######################

====================================
Example of subnetting 3
====================================

Second, write the IP address octets above the binary mask
  - If an octet of the mask is not 255 or 0, break the corresponding octet of the IP into binary as well

::

  172               .16      .00010110    .47
  11111111      .11111111    .11100000    .0


#######################

====================================
Example of subnetting 4
====================================

Third, draw a vertical line where the mask changes from 1 to 0

::

  ---------------------------------------|-------------
  172               .16             .000 | 10110    .47
  11111111      .11111111           .111 | 00000    .0 

#######################

====================================
Example of subnetting 5
====================================

The NETWORK bits are all numbers in the IP covered by 1's in the mask (to the left of your line)
  - If an octet of the mask is not 255 or 0, then your vertical line will show you which bits are part of the network. Translate it back into decimal. Assume any host bits are 0

::

  ---------NETWORK----------------|-------------
  172               .16      .000 | 10110    .47
  11111111      .11111111    .111 | 00000    .0 

Network octet is 00000000 (host bits set to 0) = 0
Network is 172.16.0.0

#######################

====================================
Example of subnetting 6
====================================

The HOST bits are all bits in the IP that are not NETWORK bits (right of your line)
If an octet of the mask is not 255 or 0, then your vertical line will show you which bits are part of the host. Translate it back into decimal. Assume any network bits are 0

::

  --------------------------------|---HOST---
  172               .16      .000 | 10110    .47
  11111111      .11111111    .111 | 00000    .0 

There are 13 host bits
  - 5 host bits in the third octet,
  - 8 in the fourth octet

#######################

====================================
Example of subnetting 7
====================================

So what?

There are now 3 network bits and 13 host bits
  - 2^N -2 hosts = (2^13)-2 = 8190 hosts per subnet
  - 2^N networks = 2^3 = 8 subnets *

The original mask of 255.255.0.0 was not disrupted. 


#######################
