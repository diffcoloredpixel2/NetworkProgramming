:title: Introduction to Network Programming
:data-transition-duration: 1500
:css: networking.css

====================================
Supplemental
====================================

Scanner Lab

    - IP range is now:
        a:c:7:9::DEF0 
        a:c:7:9::DEFF

    - Port range is now:
        5000-5020

#######################


====================================
Socket Options
====================================

Socket options allow you to specify further configuration to do interesting things

setsockopt(LEVEL, OPTION NAME, VALUE) 

Levels are either the socket itself (SOL_SOCKET) or a protocol (IPPPROTO_IPV6, etc)

Options can be found in numerous places. The mode common are in the following man pages:

    - http://man7.org/linux/man-pages/man7/socket.7.html

    - http://man7.org/linux/man-pages/man7/ip.7.html

Example:

s.setsockopt(Socket.SOL_SOCKET, SO_REUSEADDR, 1) will prevent the "Address is use" errors

#######################


====================================
IPv6 Multicasting
====================================

Use the base UDP Client and Server from lab 1

We will be doing link-local scope multicasting. Use the All nodes address 

You will edit the socket options with the level Socket.IPPROTO_IPV6

Use the following option names
    - Socket.IPV6_MULTICAST_HOPS
    - IPV6_JOIN_GROUP

Client
    - set the multicast hops to 5

Server
    - Must join an IPv6 Multicast "group"
    - Group is the packed version of the IPv6 Addr and a 32 bit int that is 0
    - see socket.inet_pton() for packing an IP addr
    - use struct.pack() with '@I' as the format string

Remember, you will recieve multicasts from each other.

Lab: Create a UDP Multicast client and server that sends (server) and recieves (client) text


#######################


====================================
Struct.pack()
====================================

Struct allows you to pack values into specified data types and sizes. Instead of manually parsing a chunk of data, struct will do it for you and provide you a list of the values.


This is how you would transfer binary data over the wire. It has an options for NETWORK BYTE ORDER

It uses a format string and variable arguments (like print or printf in C) 

See: https://docs.python.org/2.7/library/struct.html#

Lab: Connect to my server and unpack the data I send you. It is ONLY the TCP header by itself. No TCP payload or Layer 2/3. Use the TCP Header diagram.


#######################


====================================
TCP Reset Lab
====================================

Open wireshark and sniff the ens33 interface

    - Filter on port 8888

Set up a netcat listener and connector on port 8888 using IPv6

    - DON'T TYPE ANYTHING

Observe the latest packet in wireshark (TCP Handshake initially, data afterwards)

Using raw sockets, forge an ACK/RST packet from the server that kills the connector

    - You will know it worked when the connector returns to the terminal prompt. The listener will likely still be running

    - If both netcats are still running, type something on one end and verify it on the other side. Then try again using the latest packet in wireshark

Hints
    - You MUST have checksum. Use send_chksum() from the helper code
    - Seq nos and Ack nos are important!
====================================
LAB 4A - TCP Reset Lab, no data
====================================

Open wireshark and sniff the ens33 interface

    - Filter on port 8888

Set up a netcat listener and connector on port 8888 using IPv6

    - DON'T TYPE ANYTHING

Observe the latest packet in wireshark (TCP Handshake initially, data afterwards)

Using raw sockets, forge an ACK/RST packet from the server that kills the connector

    - You will know it worked when the connector returns to the terminal prompt. The listener will likely still be running

    - If both netcats are still running, type something on one end and verify it on the other side. Then try again using the latest packet in wireshark

Hints
    - You MUST have checksum. Use send_chksum() from the helper code
    - Seq nos and Ack nos are important!

BONUS:

Using Lab 4A as a base, write traffic that hijacks the connection and causes one of the netcats to print "HIJACKED"

#######################


====================================
Lab 4 Simple network Scanner
====================================

Using your previous code written:

1) Using IPv6, determine what hosts exist on the a:c:7:9::0000 to a:c:7:9::FFFF
2) Discover the MAC address of hosts that are alive
3) Discover what TCP Port numbers are open

You may NOT use python to call nmap or other utilities.

Present the results in JSON format

Host IPv6 Address
  - MAC
  - TCP port number

Scanner Lab

    - IP range is now:
        a:c:7:9::DEF0 
        a:c:7:9::DEFF

    - Port range is now:
        5000-5020

#######################