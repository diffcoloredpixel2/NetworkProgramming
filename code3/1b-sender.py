import socket   				# Import socket module
import json

udpsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  	# Create a socket object
udpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

destport = 6789  				# Port for your server
desthost = '127.0.0.1'  		# IP of the server
dest = (desthost, destport)		# The Python function sendto() requires a tuple of host,port 

senddict = {'key1':1, 'key2':'two', 'key3':'zooooom'}
msg = json.dumps(senddict)

udpsock.sendto(msg, dest)	# send the data back
print('Sent %s to %s\n' %(msg, dest))


#data, clientip = udpsock.recvfrom(256)	# recv data
#print 'Sent %s to %s\nrecv: %s from %s\n' %(msg, dest, data, clientip)