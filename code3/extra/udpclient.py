import socket   				# Import socket module

udpsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  	# Create a socket object

destport = 6789  				# Port for your server
desthost = '127.0.0.1'  		# IP of the server
dest = (desthost, destport)		# The Python function sendto() requires a tuple of host,port 

msg = "Hello"
udpsock.sendto(msg, dest)	# send the data back

data, clientip = udpsock.recvfrom(256)	# recv data

print('Sent %s to %s\nrecv: %s from %s\n' %(msg, dest, data, clientip))