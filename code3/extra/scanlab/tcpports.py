import socket
import sys   				# Import socket module

listensock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)  	# Create a socket object

myport = int(sys.argv[2] )		# Port for your server
myhost = sys.argv[1] 			# Your IP to listen on
b = (myhost, myport)			# The Python function bind() requires a tuple of host,port 
listensock.bind(b)				# Bind to the port

listensock.listen(5)			# Listen for client connection

while True:
   connsock, client = listensock.accept()  # Establish connection with client 
   
   data = connsock.recv(256)	# recv data

   print('Connection from %s. Data: %s' %(client, data))

   connsock.send(data)			# send the data back

   connsock.close()  			# Close the connection