from raw_socket_helper import RawSocket_IPv6

interfaceName = "enp3s0"
raw_socket = RawSocket_IPv6(interfaceName)


"""
need to ping6 address (or ping6 addr%<intname> for link scope) before running

mixing scopes between src, dst, and tgt DOES work

dst CAN be ff02::1
"""


srcmac = b'\x00\x0c\x29\xaa\xaa\xff' 
srcip = b'\x00\x0a\x00\x0c\x00\x07\x00\x09\x00\x00\x00\x00\x00\x00\x00\x06'
#srcip = b'\xfe\x80\x00\x00\x00\x00\x00\x00\x02\x24\x54\xff\xfe\xeb\xdc\xf2' # LINK-LOCAL ADDRESS


#dstmac = b'\xf0\xde\xf1\xad\x4b\xd6'
#dstip = b'\xfe\x80\x00\x00\x00\x00\x00\x00\x54\x69\x2c\x4c\xe7\xa5\x41\x86' # LINK-LOCAL ADDRESS 
#dstip = b'\x00\x0a\x00\x0c\x00\x07\x00\x09\x00\x00\x00\x00\x00\x00\x00\x07'

dstmac = b'\x33\x33\x00\x00\x00\x01' # layer 2 IPv6Mcast
dstip = b'\xff\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01'


#tgtip = b'\xfe\x80\x00\x00\x00\x00\x00\x00\x02\x24\x54\xff\xfe\xeb\xdc\xf2' # LINK-LOCAL ADDRESS
tgtip = b'\x00\x0a\x00\x0c\x00\x07\x00\x09\x00\x00\x00\x00\x00\x00\x00\x06'
tgtmac = b'\x00\x0c\x29\xaa\xaa\xff'

#icmp v6 options. RFC2461
icmpv6opt = b"\x02"      #type 2 = tgt link-layer addr
icmpv6opt += b'\x01'     #len = 8
icmpv6opt += tgtmac

#icmp v6
icmpv6hdr = b'\x88'          # type, neighbor advertisement
icmpv6hdr += b'\x00'         # code: 0
icmpv6hdr += b'\x00\x00'     # checksum calculated by helper code
icmpv6hdr += b'\x20' + b'\x00'*3       # RSO flags + reserved (0x20 updates addr.  0x40 sometimes adds adds REACHABLE?, 0x60 sets both)
icmpv6hdr += tgtip          # IPv6 addr that has a new Ethernet address
icmpv6 = icmpv6hdr + icmpv6opt


# IPv6 Header (aka Ethernet Payload)
ipv6hdr = b'\x60\x00\x00\x00'    # ver, traffic class, flow label (odd byte alignment if broken apart)
#ipv6hdr += b'\x00\x18'           # size of ICMPv6 Header and any options
ipv6hdr += b'\x00\x20'           # size of ICMPv6 Header and any options (+ source link layer)
ipv6hdr += b'\x3a'               # next header
ipv6hdr += b'\xff'               # hop limit
ipv6hdr += srcip
ipv6hdr += dstip
ipv6hdr += icmpv6

# Ethernet Header
dst = dstmac
src = srcmac
typ = b'\x86\xdd'    # 0x0800 is IPv4, 0x0806 is ARP, 0x86dd is IPv6

etherhdr = dst + src + typ

frame = etherhdr + ipv6hdr

for i in range(1):
    raw_socket.send_chksum(frame)
