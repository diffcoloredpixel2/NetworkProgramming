from raw_socket_helper import RawSocket

interfaceName = "ens33"
#interfaceName = "enp0s3"
raw_socket = RawSocket(interfaceName)


srcmac = b'\x00\x0c\x29\x30\x51\x17' # real hardware address of interface
srcip = b"\xc0\xa8\x00\x7b"          # src ip 

dstmac = b"\xff" * 6         # broadcast mac
dstip = b"\xc0\xa8\x00\x75"  # dst ip 

# Ethernet Header
dst = dstmac
src = srcmac
typ = b"\x08\x06"    # 0x0800 is IPv4, 0x0806 is ARP

etherhdr = dst + src + typ

# ARP Header 
arpreq = b"\x00\x01"     # type (ethernet = 1)
arpreq += b"\x08\x00"    # protocol type (IPv4 = 0x0800)
arpreq += b"\x06"        # hardware addr len bytes (Ethernet Address are 6 bytes)
arpreq += b"\x04"        # protocol addr len bytes (IPv4 Address are 4 bytes)
arpreq += b"\x00\x01"    # operation code (1 is request, 2 is reply)
arpreq += srcmac        # src hardware addr
arpreq += srcip         # src IPv4 address
arpreq += b"\x00"*6      # dst hardware addr is all 0's, we don't know it
arpreq += dstip         # dst IPv4

frame = etherhdr + arpreq # The completed frame

count = 3   # Makes it easy to find in wireshark
for i in range(count):
    raw_socket.send(frame)

#Straight Raw Sockets
#s = socket.socket(socket.PF_PACKET, socket.SOCK_RAW)
##s.bind((interfaceName, 0))
#for i in range(count):
#    frame = frame #+ "\x00" * i
#    s.send(frame)
#s.close()
