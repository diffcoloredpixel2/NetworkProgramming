import socket   				# Import socket module
import json

def reorder(mystr):
    mylist = mystr.split()
    tmpdict = {}
    ret = ''

    for word in mylist:
        tmpdict[word] = len(word)

    while len(tmpdict) > 0:
        biggest = ''
 
        for key in tmpdict:
            if tmpdict[key] > len(biggest):
                biggest = key

        ret += biggest + ' '
        del tmpdict[biggest]

    return ret


recvsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  	# Create a socket object
recvsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

myport = 6789  					# Port for your server
myhost = '127.0.0.1'  			# Your IP to listen on
b = (myhost, myport)			# The Python function bind() requires a tuple of host,port 
recvsock.bind(b)				# Bind to the port

while True:
   data, client = recvsock.recvfrom(256)	# recv data

   data2 = reorder(data)
   client2 = (client[0], client[1]+1)

   recvsock.sendto(data2, client2 )

   print('Connection from ip, port %s. Data: %s' %(client, data))
   print('\tSent back: %s to %s' %(data2, client2))