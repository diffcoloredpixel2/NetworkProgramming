import socket
import struct

###########################################################
#
#   Lab 4 Task 2 - Multicast Server
#
#       Server that can receive UDP datagrams on
#       Multicast address
#
###########################################################


# Configure address info for connection
dst_ip = "FF02::a:c:7:9"
dst_port = 12345

# Create and configure UDP socket (DGRAM)
my_sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)

# OPTIONAL: Allow multiple copies of this program on one machine
my_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Bind it to the port
my_sock.bind(('', dst_port))

group_bin = socket.inet_pton(socket.AF_INET6, dst_ip)
group = group_bin + struct.pack('@I', 0)
my_sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_JOIN_GROUP, group)

# Loop, printing any data we receive
while True:
    data, sender = my_sock.recvfrom(1500)
    while data[-1:] == '\0': data = data[:-1] # Strip trailing \0's
    print (str(sender) + ' SAYS:' + repr(data))
