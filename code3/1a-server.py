import socket   				# Import socket module

def revstr(mystr):
    strlist = mystr.split()
    revstrlist = strlist[::-1]
    return ' '.join(revstrlist)

listensock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  	# Create a socket object
listensock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)


myport = 12345  				# Port for your server
myhost = '127.0.0.1'  			# Your IP to listen on
b = (myhost, myport)			# The Python function bind() requires a tuple of host,port 
listensock.bind(b)				# Bind to the port

listensock.listen(5)			# Listen for client connection

while True:
   connsock, client = listensock.accept()  # Establish connection with client 
   
   data = connsock.recv(256)	# recv data

   print('Connection from %s. Data: %s' %(client, data))

   newdata = revstr(data)

   connsock.send(newdata)		# send the data back

   connsock.close()  			# Close the connection