import json
import socket
import subprocess

#def play(audiofile): subprocess.call(["ffplay", "-nodisp", "-autoexit", "-v", "0", audiofile])


class ForceUser(object):
    """docstring for ForceUser"""
    def __init__(self):
        super(ForceUser, self).__init__()
        self.toHit = 0
        self.damageDie = 0
        self.damageExtraInt = 0
        self.defense = 0
        self.maxHP = 0
        self.currentHP = 0
        self.maxFP = 0
        self.currentFP = 0
        self.lightSaberAttack = ""
        self.tier = 0

    def getTier(self):
        return self.tier

    def getLightSaberAttack(self):
        return self.lightSaberAttack

    def getToHit(self):
        return self.toHit

    def getDefense(self):
        return self.defense

    def getMaxHP(self):
        return self.maxHP

    def takeDmg(self, dmg):
        self.currentHP = self.currentHP-dmg

    def incFP(self):
        if self.currentFP < self.maxFP:
            self.currentFP += 1

    def decFP(self, num):
        if self.currentFP > num:
            self.currentFP = self.currentFP - num
        else:
            self.currentFP = 0

    def getMaxFP(self):
        return self.maxFP

    def setCurrentHP(self,pCurrentHP):
        self.currentHP = pCurrentHP
    def getCurrentHP(self):
        return self.currentHP

    def setCurrentFP(self,pCurrentFP):
        self.currentFP = pCurrentFP
    def getCurrentFP(self):
        return self.currentFP

    def getDamageExtraInt(self):
        return self.damageExtraInt

    def getDamageDie(self):
        return self.damageDie
    def setDamageDie(self,pDamageDie):
        self.damageDie = pDamageDie

class Youngling(ForceUser):
    """docstring for Youngling"""
    def __init__(self):
        super(Youngling, self).__init__()
        self.toHit = 1
        self.damageDie = 0
        self.damageExtraInt = 0
        self.defense = 10
        self.maxHP = 20
        self.currentHP = 20
        self.maxFP = 3
        self.currentFP = 3
        self.lightSaberAttack = "1d3 + 0"
        self.tier = 1
        
    def forcePush(self):
        if self.getCurrentFP() >= 2:
            temp = self.getCurrentFP - 2
            self.setCurrentFP(temp)
            return True
        else:
            return False

class Padawan(Youngling):
    """docstring for Padawan"""
    def __init__(self):
        super(Padawan, self).__init__()
        self.toHit = 3
        self.damageDie = 0
        self.damageExtraInt = 1
        self.defense = 12 
        self.maxHP = 30
        self.currentHP = 30
        self.maxFP = 4
        self.currentFP = 4
        self.lightSaberAttack = "1d6 + 1"
        self.tier = 2


    def heal(self):
        if self.getCurrentFP() >= 3:
            temp = self.getCurrentFP - 3
            self.setCurrentFP(temp)
            return True
        else:
            return False
        
class Knight(Padawan):
    """docstring for Knight"""
    def __init__(self):
        super(Knight, self).__init__()
        self.toHit = 6
        self.damageDie = 0
        self.damageExtraInt = 2
        self.defense = 18
        self.maxHP = 50
        self.currentHP = 50
        self.maxFP = 6
        self.currentFP = 6
        self.lightSaberAttack = "2d6 + 2"
        self.tier = 3

    def lightsaberFlurry(self):
        if self.getCurrentFP() >= 5:
            temp = self.getCurrentFP - 5
            self.setCurrentFP(temp)
            return True
        else:
            return False

class JediMaster(Knight):
    """docstring for JediMaster"""
    def __init__(self):
        super(JediMaster, self).__init__()
        self.toHit = 12
        self.damageDie = 0
        self.damageExtraInt = 3
        self.defense = 22
        self.maxHP = 75
        self.currentHP = 75
        self.maxFP = 8
        self.currentFP = 8
        self.lightSaberAttack = "3d6 + 3"
        self.tier = 4

    def drainForcePoints(self):
        if self.getCurrentFP() >= 7:
            temp = self.getCurrentFP - 7
            self.setCurrentFP(temp)
            return True
        else:
            return False        

class Acolyte(ForceUser):
    """docstring for Acolyte"""
    def __init__(self):
        super(Acolyte, self).__init__()
        self.toHit = 1
        self.damageDie = 0
        self.damageExtraInt = 0
        self.defense = 10
        self.maxHP = 20
        self.currentHP = 20
        self.maxFP = 3
        self.currentFP = 3
        self.lightSaberAttack = "1d3 + 0"
        self.tier = 1

    def forcePush(self):
        if self.getCurrentFP() >= 2:
            temp = self.getCurrentFP - 2
            self.setCurrentFP(temp)
            return True
        else:
            return False
        
class Apprentice(Acolyte):
    """docstring for Apprentice"""
    def __init__(self):
        super(Apprentice, self).__init__()
        self.toHit = 3
        self.damageDie = 0
        self.damageExtraInt = 3
        self.defense = 12
        self.maxHP = 25
        self.currentHP = 25
        self.maxFP = 4
        self.currentFP = 4
        self.lightSaberAttack = "1d6 + 3"
        self.tier = 2

    def drainLife(self):
        if self.getCurrentFP() >= 3:
            temp = self.getCurrentFP - 3
            self.setCurrentFP(temp)
            return True
        else:
            return False

class Master(Apprentice):
    """docstring for Master"""
    def __init__(self):
        super(Master, self).__init__()
        self.toHit = 6
        self.damageDie = 0
        self.damageExtraInt = 6
        self.defense = 15
        self.maxHP = 40
        self.currentHP = 40
        self.maxFP = 6
        self.currentFP = 6
        self.lightSaberAttack = "2d6 + 6"
        self.tier = 3

    def forceChoke(self):
        if self.getCurrentFP() >= 5:
            temp = self.getCurrentFP - 5
            self.setCurrentFP(temp)
            return True
        else:
            return False

class Lord(Master):
    """docstring for Lord"""
    def __init__(self):
        super(Lord, self).__init__()
        self.toHit = 12
        self.damageDie = 0
        self.damageExtraInt = 8
        self.defense = 18
        self.maxHP = 60
        self.currentHP = 60
        self.maxFP = 8
        self.currentFP = 8
        self.lightSaberAttack = "3d6 + 8"
        self.tier = 4

    def forceLighting(self):
        if self.getCurrentFP() >= 7:
            temp = self.getCurrentFP - 7
            self.setCurrentFP(temp)
            return True
        else:
            return False


class Player(object):
    """docstring for Player"""
    def __init__(self, name,side,xp):
        super(Player, self).__init__()
        #print "***************XP IS: ", xp
        self.name = name
        self.xp = xp
        self.side = side
        self.rank = ForceUser
        self.setRank()

    def getName(self):
        return self.name

    def getSide(self):
        return self.side

    def getXp(self):
        return self.xp
    def setXp(self,pXp):
        self.xp = pXp

    def getRankString(self):
        return self.rankString

    def getRank(self):
        return self.rank
    def setRank(self):
        #print "***************XP IS: ", self.xp
        if self.xp <= 199:
            if self.side == "light":
                #print "**************"
                self.rank = Youngling()
                self.rankString = "Youngling"
            else:
                self.rank = Acolyte()
                self.rankString = "Acolyte"
        elif self.xp >= 200 and self.xp <= 399:
            if self.side == "light":
                self.rank = Padawan()
                self.rankString = "Padawan"
            else:
                self.rank = Apprentice()
                self.rankString = "Apprentice"
        elif self.xp >= 400 and self.xp <= 799:
            if self.side == "light":
                self.rank = Knight()
                self.rankString = "Knight"
            else:
                self.rank = Master()
                self.rankString = "Master"
        else:
            if self.side == "light":
                self.rank = JediMaster()
                self.rankString = "JediMaster"
            else:
                self.rank = Lord()
                self.rankString = "SithLord"

    def getStat(self):
        info = []
        info.append(self.name)
        info.append(str(self.rank.getCurrentHP()))
        info.append(str(self.rank.getCurrentFP()))
        info.append(self.rank.lightSaberAttack)
        info.append(self.rankString)
        return info

#getStat() = name,currentHP,currentFP,lightsaber damage,force1,force2,...
from random import randint
def rollDie3():
    return randint(1,3)
def rollDie6():
    return randint(1,6)
def roolDie20():
    return randint(1,20)

# Function: interface
# Input: A ForcePlayer Class
# Output: string
#   Lightsaber Attack: 'Attack'
#   Force Moves: 'Force Push', 'Heal', 'Drain Life', 'Lightsaber Flurry', 'Force Choke',
#                'Drain Force Points', or 'Force Lightning'
def interface(p):
    ranks = {'Youngling':'(FP) Force Push: 1d6/2FP', 
            'Acolyte':'(FP) Force Push: 1d6/2FP',
            'Padawan':'(FP) Force Push: 1d6/2FP\n(H) Heal: 2d6+6/3FP',
            'Apprentice':'(FP) Force Push: 1d6/2FP\n(DL) Drain Life: 1d6+3/3FP',
            'Knight':'(FP) Force Push: 1d6/2FP\n(H) Heal: 2d6+6/3FP\n(LF) Lightsaber Flurry: 3d6+6/5FP',
            'Master':'(FP) Force Push: 1d6/2FP\n(DL) Drain Life: 1d6+3/3FP\n(FC) Force Choke: 3d6+6/5FP',
            'JediMaster':'(FP) Force Push: 1d6/2FP\n(H) Heal: 2d6+6/3FP\n(LF) Lightsaber Flurry: 3d6+6/5FP\n(DFP) Drain Force Points: Negate Enemy FP',
            'SithLord':'(FP) Force Push: 1d6/2FP\n(DL) Drain Life: 1d6+3/3FP\n(FC) Force Choke: 3d6+6/5FP\n(FL) Force Lightning:  5d6+5,7FP'}

    # Get forceUserinput loop
    while True:

        # Get Stats
        statlist = p.getStat()
        fp = int(statlist[2])
        rank = statlist[4]

        # Build request string Name and Stats
        string = '{} {} - HP: {} FP: {}\n'.format(statlist[0], statlist[4], statlist[1], statlist[2])
        # Add action selection
        string += 'Select action: \n'
        # Add Attack and Force info 
        string += '(A) Lightsaber Attack: {}\n'.format(statlist[3])
        # Add Force Moves
        for f in ranks[statlist[4]]:
            string += f

        string += '\nselect > '

        # Get forceUser input
        #forceUser_input = raw_input(string)
        forceUser_input = 'A'
        if forceUser_input == 'a' or forceUser_input == 'A':            # Attack
            return ['Attack', statlist[3]]

        if forceUser_input == 'fp' or forceUser_input == 'FP':          # Force Push
            if fp >= 2: 
                #p.getRank().decFP(2)
                return ['Force Push',statlist[3]]
            else: print("Unable to use Force")

        if forceUser_input == 'h' or forceUser_input == 'H':            # Heal
            if fp >= 3 and (rank == 'Padawan' or rank == 'Knight' or rank == 'JediMaster'): 
                #p.getRank().decFP(3)
                return ['Heal',statlist[3]]
            else: print("Unable to use Force")

        if forceUser_input == 'dl' or forceUser_input == 'DL':          # Drain Life
            if fp >= 3 and (rank == 'Apprentice' or rank == 'Master' or 'SithLord'): 
                #p.getRank().decFP(3)
                return ['Drain Life',statlist[3]]
            else: print("Unable to use Force")

        if forceUser_input == 'lf' or forceUser_input == 'LF':          # Lightsaber Flurry
            if fp >= 5 and (rank == 'Knight' or rank == 'JediMaster'): 
                #p.getRank().decFP(5)
                return ['Lightsaber Flurry',statlist[3]]
            else: print("Unable to use Force")

        if forceUser_input == 'fc' or forceUser_input == 'FC':          # Force Choke
            if fp >= 5 and (rank == 'Master' or 'SithLord'): 
                #p.getRank().decFP(5)
                return ['Force Choke',statlist[3]]
            else: print("Unable to use Force")

        if forceUser_input == 'dfp' or forceUser_input == 'DFP':        # Drain Force Points
            if fp >= 7 and rank == 'JediMaster': 
                #p.getRank().decFP(7)
                return ['Drain Force Points',statlist[3]]
            else: print("Unable to use Force")

        if forceUser_input == 'fl' or forceUser_input == 'FL':          # Force Lightning
            if fp >= 7 and 'SithLord': 
                #p.getRank().decFP(7)
                return ['Force Lightning',statlist[3]]
            else: print("Unable to use Force")
       
        print("")

def login(name, passwrd, s):
    print("USE NETWORK JSON RETURN EXP and Alignment")
    #data[] = [name, passwrd]
    
    data = {"Username":name, "Password":passwrd}
    sdata = json.dumps(data)
    
    #temp = '''{'Username': 'Test', 'Password': 'Testa'}'''
    s.send(sdata)
    temp = json.loads(sdata)

    print("TEST")
    print(temp)
    
    rdata = s.recv(1024)
    ans = json.loads(rdata)
    print(ans)
    
    if ans['Alignment'] == "Jedi":
        ans['Alignment'] = 'light'
    else :
        ans['Alignment'] = 'dark'

    charinfo = [ans['Alignment'], int(ans['XP'])]
    return charinfo

def sendmov(movtype, movdmg, playr, s):
    print("USE JSON TO SEND CHARACTER MOVE")

    data = {"MoveName": movtype, "CharName":playr.getName(), "HP":playr.getRank().getCurrentHP(), "FP":playr.getRank().getCurrentFP()}
    print(data)
    sdata = json.dumps(data)

    s.send(sdata)

    rdata = s.recv(1024)       #Says whether or not you accept your move
    print("***********UNLOADED CHECK DATA**********")
    print(rdata)

    ans2 = {}
    splitme = rdata.split("}{")
    if len(splitme) == 2:
        splitme[0] = splitme[0]+'}'
        splitme[1] = '{' + splitme[1]

        checkret = json.loads(splitme[0])
        print(checkret)
        ans2 = json.loads(splitme[1])
        print(ans2)
    else:
        ans2 = json.loads(rdata)
        print(ans2)

        rdata = s.recv(1024)       #Says the result of move
        print("***********UNLOADED MOVE DATA**********")
        print(rdata)
        ans2 = json.loads(rdata)
        print(ans2)

    #ans = {'dmgrec':2, 'dmggiv':4, 'fpused':0, 'enemyfpused':1, 'enemyMov':"Force Push", 'xpgain':0}
    return ans2

def registerchar(usern, passw, align, s):
    print("USE JSON TO REGISTER USER")
    data = {'Username':usern, 'Password':passw, 'Alignment':align}
    sdata = json.dumps(data)
    s.send(sdata)
    rdata = s.recv(1024)
    print(json.loads(rdata))
    return "OKAY"

def playsound(movenam):
    if movenam == 'Attack':
        play('LightSaber1.wav')
    elif movenam == 'Force Push':
        play('ForcePush.wav')
    elif movenam == 'Heal':
        play('HealJedi.wav')
    elif movenam == 'Drain Life':
        play('DrainLife.wav')
    elif movenam == 'Lightsaber Flurry':
        play('LightSaberFlurry.wav')
    elif movenam == 'Force Choke':
        play('ForceChoke.wav')
    elif movenam == 'Drain Force Points':
        play('DrainForcePoints.wav')
    elif movenam == 'Force Lightning':
        play('ForceLightning.wav')
    else:
        print("****NOMOVE*****")
            
    
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('192.168.1.146',4242)) 

print("WELCOME TO STARWARS!!!")
#play('Theme.wav')
print("Login (l) or New Character (n): ")
while True:
    #choice = raw_input()
    choice = 'l'
    if choice == 'l':
        print("Enter Username: ")
        #name = raw_input()
        name = 'aaa'
        print("Enter Password: ")
        #passwrd = raw_input()
        passwrd = 'bbb'
        charinfo = login(name, passwrd, s)  #CORRECT LINE
       # charinfo = login("aa", "bb", s)

        #charinfo = ['light', 850] #fake
        print(charinfo)
        
        playr = Player(name, charinfo[0], charinfo[1])

        bdata = s.recv(1024)  #Need this
        print("***BDATA***")
        print(bdata)

        battle = True
        while battle:
            print()
            print("GETTING MOVE")
            #print
            #print json.loads(bdata)

            mov = interface(playr)
            print("Move is: ", mov)
            ans = sendmov(mov[0],mov[1], playr, s) #Correct
            #ans={'EnemyDamageTaken':0, 'EnemyMove':"Blahblah", 'DamageTaken':0, 'FPUsed':0, 'XP':0}
            
            print()
            print("You used ",mov[0])
            #playsound(mov[0])
            print("You Dealt ", int(ans['EnemyDamageTaken']), " Damage!")
            print("Enemy Used ", ans['EnemyMove'])
            print("You Took ", ans['DamageTaken'], " Damage!")
            #playr.getRank().takeDmg(int(ans['dmgrec'])
            playr.getRank().takeDmg(int(ans['DamageTaken']))
            playr.getRank().decFP(int(ans['FPUsed']))
            if float(ans['XP']) == 0:
                print("Do another move")
                playr.getRank().incFP()
            else:
                print("Battle Over!")
                print("My HP is: ",playr.getRank().getCurrentHP())
                if playr.getRank().getCurrentHP() > 0:
                    print("You WON!")
                else:
                    print("You Lost!")
                print("You received ", float(ans['XP']), " experience")
                battle = False
                print()
                print("Login (l) or New Character (n): ")

        break
    elif choice == 'n':
        print("Enter New Username: ")
        name = input()
        print("Enter New Password: ")
        passwrd = input()
        print("Choose Alignment (Jedi or Sith): ")
        align = input()
        #print "Info is: ",name, " ", passwrd, " ", align
        registerchar(name, passwrd, align, s)
    else:
        print("Wrong input")
        print("Login (l) or New Character (n): ")
s.close()