from forceuser import ForceUser

class Acolyte (ForceUser):
    def __init__(self=None, toHit=1, dmg=(1,3,0), defense=10, hp=20, fp=3, abilityList={} ):
        abilityList.update( {"FORCE PUSH": (2, (1,6,0))} )
        ForceUser.__init__(self, toHit, dmg, defense, hp, fp, abilityList)

class Apprentice (Acolyte):
    def __init__(self=None, toHit=3, dmg=(1,6,3), defense=12, hp=25, fp=4, abilityList={}):
        abilityList.update( { "DRAIN LIFE": (3, (1, 6, 3)) } )
        Acolyte.__init__(self, toHit, dmg, defense, hp, fp, abilityList)

class SithMaster (Apprentice):
    def __init__(self=None, toHit=6, dmg=(2,6,6), defense=15, hp=40, fp=6, abilityList={}):
        abilityList.update( { "FORCE CHOKE": (5, (3, 6, 6)) } )
        Apprentice.__init__(self, toHit, dmg, defense, hp, fp, abilityList)

class SithLord (SithMaster):
    def __init__(self=None, toHit=12, dmg=(3,6,8), defense=18, hp=60, fp=8, abilityList={}):
        abilityList.update( { "FORCE LIGHTNING": (7, (5, 6, 5)) } )
        SithMaster.__init__(self, toHit, dmg, defense, hp, fp, abilityList)

