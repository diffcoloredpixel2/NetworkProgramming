from random import randint

def roll(cnt, dice):
    dicesum = 0
    #print "\trolling...",
    for i in range(0, cnt):
        x = randint(1, dice)
        dicesum += x
        #print x,
    #print 
    #print "\tdicesum = %d" %(dicesum)
    return dicesum

def calculateDamage(dmgTuple):
    #print "%dD%d + %d" %(dmgTuple[0], dmgTuple[1], dmgTuple[2])
    return roll(dmgTuple[0], dmgTuple[1]) + dmgTuple[2]

def makeMove(char):
    forceMoves = list(char.getForceAbilities().keys())

    x = randint(0, len(forceMoves) + 1)

    try:
        return forceMoves[x]

    except IndexError:
        return 'REGULAR ATTACK'

def resolve(aMove, a, bMove, b):

    # DMG to A
    # DMG to B
    # A's FP used
    # B's FP used
    # A's move 
    # B's move
    # A's XP Gain
    # B's XP gain




    aDmg = 0
    bDmg = 0

    printA = True
    printB = True

    print("")

    #Start with caveats

    # Drain FP         
    if 'Drain Force Points'.upper() == aMove.upper() and 'Drain Force Points'.upper() == bMove.upper():
        #both players cast Drain Force Points
        print("(dfp) A is using %s" %(aMove))
        print("(dfp) B is using %s" %(bMove))
        #a.useForceAbility(aMove)
        #b.useForceAbility(bMove)
        b.depleteFP(7)
        a.depleteFP(7)            
        return aDmg, bDmg

    elif 'Drain Force Points'.upper() == aMove.upper() and 'Regular Attack'.upper() != bMove.upper():
        # A casts Drain FP, B is casting a force power which will be interupted
        print("(dfp) A is using %s" %(aMove))
        printA = False
        combatTesterForceAbility(a, aMove)
        print("(dfp) B's Force Attack was negated") 
        b.depleteFP(7)
        return aDmg, bDmg           

    elif 'Drain Force Points'.upper() == bMove.upper() and 'Regular Attack'.upper() != aMove.upper():
        # B casts Drain FP, A is casting a force power which will be interupted
        print("(dfp) B is using %s" %(bMove))
        printB = False
        combatTesterForceAbility(b, bMove)
        print("(dfp) A's Force Attack was negated") 
        a.depleteFP(7) 
        return aDmg, bDmg           

    else:
        pass


    #Jedi Heal
    if 'Heal'.upper() == aMove.upper():
        print("(jh) A is using %s" %(aMove))
        printA = False
        heal = combatTesterForceAbility(a, aMove)
        a.heal(heal)

    elif 'Heal'.upper() == bMove.upper():
        print("(jh) B is using %s" %(bMove))
        printB = False
        heal = combatTesterForceAbility(b, bMove)
        b.heal(aDmg)

    #Sith Drain Life
    if 'Drain Life'.upper() == aMove.upper():
        print("(dl) A is using %s" %(aMove))
        printA = False
        aDmg = combatTesterForceAbility(a, aMove)
        a.heal(aDmg)
        

    elif 'Drain Life'.upper() == bMove.upper():
        print("(dl) B is using %s" %(bMove))
        printB = False
        bDmg = combatTesterForceAbility(b, bMove)                      
        b.heal(bDmg)


    ################
    # NORMAL
    ################

    #A MOVE
    if printA:
        print("A is using %s" %(aMove))

        if 'Regular Attack'.upper() == aMove.upper(): 
            aDmg = combatTesterBasicAttack(a,b) 

        else:  
            try:
                aDmg = combatTesterForceAbility(a, aMove)

            except ValueError as err:
                print((err.args))
                #print "A does not have enough FP, doing a regular attack"
                #aDmg = combatTesterBasicAttack(a,b)
                input("Press Enter to Continue\n")
    
    

    # B MOVE
    if printB:
        print("B is using %s" %(bMove))

        if 'Regular Attack'.upper() == bMove.upper(): 
            bDmg = combatTesterBasicAttack(b,a) 

        else:  
            try:
                bDmg = combatTesterForceAbility(b, bMove)

            except ValueError as err:
                print((err.args))
                #print "B does not have enough FP, doing a regular attack"
                #bDmg = combatTesterBasicAttack(b,a)
                input("Press Enter to Continue\n")
                
    
    return aDmg, bDmg