import socket
import select
import json
import sys

import warstaruser

from youngling import Youngling
from acolyte import Acolyte

from padawan import Padawan
from apprentice import Apprentice

from jediknight import JediKnight
from sithmaster import SithMaster

from jedimaster import JediMaster
from sithlord import SithLord

from battleutil import *


combatdict1 = None
combatdict2 = None

combat1 = None
combat2 = None

player1connsock = None
player1addr = ''
player2connsock = None
player2addr = ''

player1move = ''
player2move = ''


def resolve(socka, sockb):
	print("WERTY")
	d = { 'DamageTaken':0, 'EnemyDamageTaken':0, 'FPUsed':0, 'EnemyFPUsed':0, 'EnemyMove':"blahblah", 'XP':0}

	sendback = json.dumps( d )
	print("\tSendback: " + sendback)
	socka.send(sendback)
	sockb.send(sendback)



def recvmove(clisock, address, player):
	ret = None
	tmp = None
	
	print("Battle Connection from: " + str( address ))

	alldata = ''
	data = clisock.recv(1)
	alldata = alldata + data
	#print '\t' + data,
	while len(data) > 0:
		data = clisock.recv(1)	
		#print data,			
		alldata = alldata + data		
		if data == '}':
			break

	print("\t" + alldata)

	try:		
		unjdata = json.loads(alldata)		
		if player == 1:
			player1move =  unjdata
		else:
			player2move = unjdata
		messagedict = {'Result': 'Success'}

	except:	
		e = sys.exc_info()[0]
		print(e)	
		messagedict = {"Result": "Failure JSON.loads() "}
		return False

	

	#d = { 'DamageTaken':0, 'EnemyDamageTaken':0, 'FPUsed':0, 'EnemyFPUsed':0, 'EnemyMove':"blahblah", 'XP':0}

	

	sendback = json.dumps( messagedict )
	print("\tSendback (%s): %s" %(address, sendback))
	clisock.send(sendback)

	return True;

def recvlogin(clisock, address):

	ret = None
	tmp = None
	
	print("Connection from: " + str( address ))

	alldata = ''
	data = clisock.recv(1)
	alldata = alldata + data
	#print '\t' + data,
	while len(data) > 0:
		data = clisock.recv(1)	
		#print data,			
		alldata = alldata + data		
		if data == '}':
			break

	print("\t" + alldata)

	messagedict = {"Result": "Initial Failure "}

	try:		
		unjdata = json.loads(alldata)		
		messagedict = warstaruser.login(unjdata['Username'], unjdata['Password'])

		if messagedict['Result'] == 'Success':
			ret = True
			tmp = {'Result': messagedict['Result'], 'Alignment': messagedict['Alignment'], 'XP': messagedict['XP']  }
	

	except:	
		e = sys.exc_info()[0]
		print(e)	
		messagedict = {"Result": "Failure JSON.loads() "}

	sendback = json.dumps( tmp )
	print("\tSendback: " + sendback)
	clisock.send(sendback)
	return (ret, messagedict)


def makeForceUser(thedict):
	#Tier 1: 0-199 XP
	#Tier 2: 200-399 XP
	#Tier 3: 400-799 XP
	#Tier 4: 800+ XP

	align = thedict['Alignment']
	xp = thedict['XP']

	if xp < 200:
		if align == 'Jedi':
			return Youngling(1, (1,3,0), 10, 20, 3)
		elif align == 'Sith':
			return Acolyte(1, (1,3,0), 10, 20, 3)
		else:
			print("WTF1")
			sys.exit(1)
	elif xp < 400:
		if align == 'Jedi':
			return Padawan(3, (1,6,1), 12, 30, 4)
		elif align == 'Sith':
			return Apprentice(3, (1,6,3), 12, 25, 4)
		else:
			print("WTF2")
			sys.exit(1)
	elif xp < 600:
		if align == 'Jedi':
			return JediKnight(6, (2,6,2), 18, 50, 6)
		elif align == 'Sith':
			return SithMaster(6, (2,6,6), 15, 40, 6)
		else:
			print("WTF2")
			sys.exit(1)
	else:
		if align == 'Jedi':
			return JediMaster(12, (3,6,3), 22, 75, 8)
		elif align == 'Sith':
			return SithLord(12, (3,6,8), 18, 60, 8)
		else:
			print("WTF2")
			sys.exit(1)





p1port = 2345
p1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
p1.setblocking(0)
p1.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
p1.bind( ('', p1port) )
p1.listen(1)

p2port = 2346
p2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
p2.setblocking(0)
p2.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
p2.bind( ('', p2port) ) 
p2.listen(1)


# Handle Logins
while True:

	#raw_input('press enter to continue')

	
	print("@@@@@@@@@ LOGIN @@@@@@@@@@@@@@@@")

	p1login = False
	p2login = False

	inputs = []


	if not p1login:
		inputs.append(p1)
	if not p2login:
		inputs.append(p2)

	# Sockets to which we expect to read
	inputs = [ p1, p2 ]
	# Sockets to which we expect to write
	outputs = [ ]

	while True:
		readable, writable, exceptional = select.select(inputs, outputs, inputs)

		for s in readable:

			#print '----------------'
			#print "p1 login? " + str(p1login)
			#print "p2 login? " + str(p2login)
			#print "S == p1? " + str(s==p1)
			#print "S == p2? " + str(s==p2)

			if s == p1 and not p1login:
				clisock1, address = p1.accept()
				q = recvlogin(clisock1, address) 
				if q[0]:
					p1login = True
					player1connsock = clisock1
					inputs.remove(p1)
					combatdict1 = q[1]
					combat1 = makeForceUser(combatdict1)
					player1addr = address

			if s == p2 and not p2login:
				clisock2, address = p2.accept()
				q = recvlogin(clisock2, address) 
				if q[0]:
					p2login = True
					player2connsock = clisock2
					combatdict2 = q[1]
					combat2 = makeForceUser(combatdict2)
					player2addr = address

		

		if p1login and p2login:
			
			print("BOTH LOGIN SUCCESS")


			#RESET
			if False:
				player1connsock.send('\nHello %s. 2 players connected. Goodbye' %(combatdict1['Name']) )
				#player1connsock.flush()
				player1connsock.close()

				player2connsock.send('\nHello %s. 2 players connected. Goodbye' %(combatdict2['Name']) )
				#player2connsock.flush()
				player2connsock.close()

				p1login = False
				p2login = False

				player1connsock = None
				player2connsock = None
			
			break
			
				
	if p1login and p2login:
			
		player1connsock.send('\nHello %s. 2 players connected.' %(combatdict1['Name']) )
		#player1connsock.flush()
		
		player2connsock.send('\nHello %s. 2 players connected.' %(combatdict2['Name']) )
		#player2connsock.flush()
		print("Battling...")
		break
#Pass name

roundcnt = 1
while True:

	print("@@@@@@@@@ Battle @@@@@@@@@@@@@@@@")

	p1move = False
	p2move = False
	
	inputs = []

	# Sockets to which we expect to read
	if not p1move:
		inputs.append( player1connsock )
	if not p2move:
		inputs.append( player2connsock )

	# Sockets to which we expect to write
	outputs = []

	while True:
		readable, writable, exceptional = select.select(inputs, outputs, inputs)

		for s in readable:
			#print inputs

			#print '----------------'
			#print "p1 login? " + str(p1login)
			#print "p2 login? " + str(p2login)
			#print "S == p1? " + str(s==p1)
			#print "S == p2? " + str(s==p2)

			if s == player1connsock and not p1move:
				#clisock1, address = p1.accept()
				q = recvmove(player1connsock, player1addr, 1) 

				if q:
					p1move = True
					inputs.remove(player1connsock)
				

			if s == player2connsock and not p2move:
				#clisock2, address = p2.accept()
				q = recvmove(player2connsock, player2addr, 2)
				if q:
					p2move = True
					inputs.remove(player2connsock)

			if p1move and p2move:
				print("ROUND %d RESOLVE" %(roundcnt))
				resolve(player1connsock, player2connsock)

				p1move = False
				p2move = False
				roundcnt = roundcnt + 1

				


#BATTLE!