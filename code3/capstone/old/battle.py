

from youngling import Youngling
from acolyte import Acolyte

from padawan import Padawan
from apprentice import Apprentice

from jediknight import JediKnight
from sithmaster import SithMaster

from jedimaster import JediMaster
from sithlord import SithLord

from battleutil import *

#ytuplelist = [("Force Push", 2, (1, 6, 1))]
#ytuplelist = []

def genChars(tierA=1, jediA=True, tierB=1, jediB=False):

    if tierA > 4 or tierA < 1 or tierB > 4 or tierB < 1:
        raise ValueError("Tier not between 1 and 4", tierA, tierB)

    if 1 == tierA:
        if jediA:
            a = Youngling(1, (1,3,0), 10, 20, 3)
        else:
            a = Acolyte(1, (1,3,0), 10, 20, 3)
    elif 2 == tierA:
        if jediA:
            a = Padawan(3, (1,6,1), 12, 30, 4)
        else:
            a = Apprentice(3, (1,6,3), 12, 25, 4)
    elif 3 == tierA:
        if jediA:
            a = JediKnight(6, (2,6,2), 18, 50, 6)
        else:
            a = SithMaster(6, (2,6,6), 15, 40, 6)
    elif 4 == tierA:
        if jediA:
            a = JediMaster(12, (3,6,3), 22, 75, 8)
        else:
            a = SithLord(12, (3,6,8), 18, 60, 8)
    else:
        pass


    if 1 == tierB:
        if jediB:
            b =  Youngling(1, (1,3,0), 10, 20, 3)
        else:
            b =  Acolyte(1, (1,3,0), 10, 20, 3)
    elif 2 == tierB:
        if jediB:
            b =  Padawan(3, (1,6,1), 12, 30, 4)
        else:
            b =  Apprentice(3, (1,6,3), 12, 25, 4)
    elif 3 == tierB:
        if jediB:
            b =  JediKnight(6, (2,6,2), 18, 50, 6)
        else:
            b =  SithMaster(6, (2,6,6), 15, 40, 6)
    elif 4 == tierB:
        if jediB:
            b =  JediMaster(12, (3,6,3), 22, 75, 8)
        else:
            b =  SithLord(12, (3,6,8), 18, 60, 8)
    else:
        pass
        
    return a,b






def combatTesterForceAbility(a, abilityName = ""):
    aForce = a.useForceAbility(abilityName)

    if 'Drain Force Points'.upper() == abilityName.upper():
        return 0

    aDmg = calculateDamage(aForce)
    print("\tDmg = %d" %(aDmg))
    return aDmg


def combatTesterBasicAttack(a, b): 
        aBonus = a.getToHitBonus()
        aRoll = roll(1, 20)
        aVal = aRoll + aBonus
        print("\tRolled %d + Bonus %d = To Hit %d" %(aRoll, aBonus, aVal))

        bDef = b.getDefense()
        aDmg = 0

        if aVal < bDef:
            print("\tMISS :(")
        else:
            aDmg = calculateDamage(a.getDamage())
            print("\tDmg = %d" %(aDmg))

        return aDmg

def resolve(aMove, a, bMove, b):

        aDmg = 0
        bDmg = 0

        printA = True
        printB = True

        print("")

        #Start with caveats

        # Drain FP         
        if 'Drain Force Points'.upper() == aMove.upper() and 'Drain Force Points'.upper() == bMove.upper():
            #both players cast Drain Force Points
            print("(dfp) A is using %s" %(aMove))
            print("(dfp) B is using %s" %(bMove))
            #a.useForceAbility(aMove)
            #b.useForceAbility(bMove)
            b.depleteFP(7)
            a.depleteFP(7)            
            return aDmg, bDmg

        elif 'Drain Force Points'.upper() == aMove.upper() and 'Regular Attack'.upper() != bMove.upper():
            # A casts Drain FP, B is casting a force power which will be interupted
            print("(dfp) A is using %s" %(aMove))
            printA = False
            combatTesterForceAbility(a, aMove)
            print("(dfp) B's Force Attack was negated") 
            b.depleteFP(7)
            return aDmg, bDmg           

        elif 'Drain Force Points'.upper() == bMove.upper() and 'Regular Attack'.upper() != aMove.upper():
            # B casts Drain FP, A is casting a force power which will be interupted
            print("(dfp) B is using %s" %(bMove))
            printB = False
            combatTesterForceAbility(b, bMove)
            print("(dfp) A's Force Attack was negated") 
            a.depleteFP(7) 
            return aDmg, bDmg           

        else:
            pass


        #Jedi Heal
        if 'Heal'.upper() == aMove.upper():
            print("(jh) A is using %s" %(aMove))
            printA = False
            heal = combatTesterForceAbility(a, aMove)
            a.heal(heal)

        elif 'Heal'.upper() == bMove.upper():
            print("(jh) B is using %s" %(bMove))
            printB = False
            heal = combatTesterForceAbility(b, bMove)
            b.heal(aDmg)

        #Sith Drain Life
        if 'Drain Life'.upper() == aMove.upper():
            print("(dl) A is using %s" %(aMove))
            printA = False
            aDmg = combatTesterForceAbility(a, aMove)
            a.heal(aDmg)
            

        elif 'Drain Life'.upper() == bMove.upper():
            print("(dl) B is using %s" %(bMove))
            printB = False
            bDmg = combatTesterForceAbility(b, bMove)                      
            b.heal(bDmg)


        ################
        # NORMAL
        ################

        #A MOVE
        if printA:
            print("A is using %s" %(aMove))

            if 'Regular Attack'.upper() == aMove.upper(): 
                aDmg = combatTesterBasicAttack(a,b) 

            else:  
                try:
                    aDmg = combatTesterForceAbility(a, aMove)

                except ValueError as err:
                    print((err.args))
                    #print "A does not have enough FP, doing a regular attack"
                    #aDmg = combatTesterBasicAttack(a,b)
                    input("Press Enter to Continue\n")
        
        

        # B MOVE
        if printB:
            print("B is using %s" %(bMove))

            if 'Regular Attack'.upper() == bMove.upper(): 
                bDmg = combatTesterBasicAttack(b,a) 

            else:  
                try:
                    bDmg = combatTesterForceAbility(b, bMove)

                except ValueError as err:
                    print((err.args))
                    #print "B does not have enough FP, doing a regular attack"
                    #bDmg = combatTesterBasicAttack(b,a)
                    input("Press Enter to Continue\n")
                    
        
        return aDmg, bDmg

def combat():
    
    a,b = genChars(4, True, 4, False)
    

    a.startCombat()
    b.startCombat()

    roundCnt = 1

    print("@@@@@@@@@@@@@@@@@@@@@@@@@") 

    while True:

        print("\n========Round %d=========\n" %(roundCnt))
        roundCnt += 1

        # Regenerate Force Points at begin of round
        a.regenFP()
        b.regenFP()

        print("A HP: %d, FP: %d" %( a.getCurrentHP(), a.getCurrentFP() ))
        print("B HP: %d, FP: %d" %( b.getCurrentHP(), b.getCurrentFP() ))
        
        # Select move for each character

        while True:
            aMove = makeMove(a)
            if 'REGULAR ATTACK' == aMove:
                break
            if a.hasEnoughFP(aMove):
                break
            #print "A lacks the FP to cast %s (need %d, have %d). Choose another attack" %(aMove, a.abilityFPCost(aMove), a.getCurrentFP() )

        while True:
            bMove = makeMove(b)
            if 'REGULAR ATTACK' == bMove:
                break
            if b.hasEnoughFP(bMove):
                break
            #print "B lacks the FP to cast %s (need %d, have %d). Choose another attack" %(bMove, b.abilityFPCost(bMove), b.getCurrentFP() )

        aDmg, bDmg = resolve(aMove, a, bMove, b)


        #take damage (dmg may be 0)        
        a.takeDamage(bDmg)
        b.takeDamage(aDmg)

        #print ""
        #print "A HP: %d, FP: %d" %( a.getCurrentHP(), a.getCurrentFP() )
        #print "B HP: %d, FP: %d" %( b.getCurrentHP(), b.getCurrentFP() )

        if a.getCurrentHP() <= 0 or b.getCurrentHP() <= 0:
            break

        if roundCnt > 300:
            print("roundCnt break")
            break

        #raw_input('\nPress Enter to continue')
        

    if a.getCurrentHP() <= 0 and b.getCurrentHP() <= 0:
        print("\nDraw")

    elif a.getCurrentHP() > 0 and b.getCurrentHP() <= 0:
        print("\nA wins")

    elif a.getCurrentHP() <= 0 and b.getCurrentHP() > 0:
        print("\nB wins")
    
    else:
        print("\nlol wut?")


combat()

