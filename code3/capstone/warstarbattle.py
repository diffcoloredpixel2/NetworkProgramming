from random import randint  






def calcXPTie(aLevel, bLevel):
    #Tie = 0.75 + (.10 * (Opponent Tier - Your Tier + 1))
    modXPA = 0.75 + (.10 * (bLevel - aLevel + 1))
    modXPB = 0.75 + (.10 * (aLevel - bLevel + 1))

    tieXPA = 100 * modXPA
    tieXPB = 100 *modXPB



    return tieXPA, tieXPB

def calcXP(winlevel, loselevel):
    #Win = 1.00 + (.20 * (Loser Tier - Winner Tier + 1))
    #Loss = 0.50 + (.05 * (Loser Tier - Winner Tier + 1))
    

    winmod = 1 + (.2 * (loselevel - winlevel + 1))
    losemod = 0.5 + (.05 * (winlevel - loselevel + 1))

    winXP = 100 * winmod
    loseXP = 100 * losemod

    return winXP, loseXP

    pass


def roll(cnt, dice):
    dicesum = 0
    #print "\trolling...",
    for i in range(0, cnt):
        x = randint(1, dice)
        dicesum += x
        #print x,
    #print 
    #print "\tdicesum = %d" %(dicesum)
    return dicesum

def calculateDamage(dmgTuple):
    #print "%dD%d + %d" %(dmgTuple[0], dmgTuple[1], dmgTuple[2])
    return roll(dmgTuple[0], dmgTuple[1]) + dmgTuple[2]

def makeMove(char):
    forceMoves = list(char.getForceAbilities().keys())

    x = randint(0, len(forceMoves) + 1)

    try:
        return forceMoves[x]

    except IndexError:
        return 'Attack'

def combatTesterForceAbility(a, abilityName = ""):
    aForce = a.useForceAbility(abilityName)

    if 'Drain Force Points'.upper() == abilityName.upper():
        return 0

    aDmg = calculateDamage(aForce)
    print("\tDmg = %d" %(aDmg))
    return aDmg


def combatTesterBasicAttack(a, b): 
        aBonus = a.getToHitBonus()
        aRoll = roll(1, 20)
        aVal = aRoll + aBonus
        print("\tRolled %d + Bonus %d = To Hit %d" %(aRoll, aBonus, aVal))

        bDef = b.getDefense()
        aDmg = 0

        if aVal < bDef:
            print("\tMISS :(")
        else:
            aDmg = calculateDamage(a.getDamage())
            print("\tDmg = %d" %(aDmg))

        return aDmg

def resolve(aMove, a, aName, bMove, b, bName):

        aDmg = 0
        bDmg = 0

        printA = True
        printB = True

        #Start with caveats

        # Drain FP         
        if 'Drain Force Points'.upper() == aMove.upper() and 'Drain Force Points'.upper() == bMove.upper():
            #both players cast Drain Force Points
            print("  (dfp) %s is using %s" %(aName, aMove))
            print("  (dfp) %s is using %s" %(bMove, bMove))
            #a.useForceAbility(aMove)
            #b.useForceAbility(bMove)
            b.depleteFP(7)
            a.depleteFP(7)

            return aDmg, bDmg

        elif 'Drain Force Points'.upper() == aMove.upper() and 'Attack'.upper() != bMove.upper():
            # A casts Drain FP, B is casting a force power which will be interupted
            print("  (dfp) %s is using %s" %(aName, aMove))
            printA = False
            combatTesterForceAbility(a, aMove)
            print("  (dfp) %s's Force Attack was negated" %(bMove))
            b.depleteFP(7)
            return aDmg, bDmg           

        elif 'Drain Force Points'.upper() == bMove.upper() and 'Attack'.upper() != aMove.upper():
            # B casts Drain FP, A is casting a force power which will be interupted
            print("  (dfp) %s is using %s" %(bName, bMove))
            printB = False
            combatTesterForceAbility(b, bMove)
            print("  (dfp) %s's Force Attack was negated" %(aName)) 
            a.depleteFP(7) 
            return aDmg, bDmg           

        else:
            pass


        #Jedi Heal
        if 'Heal'.upper() == aMove.upper():
            print("  (jh) %s is using %s" %(aName, aMove))
            printA = False
            heal = combatTesterForceAbility(a, aMove)
            a.heal(heal)

        elif 'Heal'.upper() == bMove.upper():
            print("  (jh) %s is using %s" %(bName, bMove))
            printB = False
            heal = combatTesterForceAbility(b, bMove)
            b.heal(aDmg)

        #Sith Drain Life
        if 'Drain Life'.upper() == aMove.upper():
            print("  (dl) %s is using %s" %(aName, aMove))
            printA = False
            aDmg = combatTesterForceAbility(a, aMove)
            a.heal(aDmg)
            

        elif 'Drain Life'.upper() == bMove.upper():
            print("  (dl) %s is using %s" %(bName, bMove))
            printB = False
            bDmg = combatTesterForceAbility(b, bMove)                      
            b.heal(bDmg)


        ################
        # NORMAL
        ################

        #A MOVE
        if printA:
            print("  %s is using %s" %(aName, aMove))

            if 'Attack'.upper() == aMove.upper(): 
                aDmg = combatTesterBasicAttack(a,b) 

            else:  
                try:
                    aDmg = combatTesterForceAbility(a, aMove)

                except ValueError as err:
                    print((err.args))
                    #print "A does not have enough FP, doing a regular attack"
                    #aDmg = combatTesterBasicAttack(a,b)
                    #raw_input("Press Enter to Continue\n")
        
        

        # B MOVE
        if printB:
            print("  %s is using %s" %(bName, bMove))

            if 'Attack'.upper() == bMove.upper(): 
                bDmg = combatTesterBasicAttack(b,a) 

            else:  
                try:
                    bDmg = combatTesterForceAbility(b, bMove)

                except ValueError as err:
                    print((err.args))
                    #print "B does not have enough FP, doing a regular attack"
                    #bDmg = combatTesterBasicAttack(b,a)
                    #raw_input("Press Enter to Continue\n")
                    
        
        return aDmg, bDmg


