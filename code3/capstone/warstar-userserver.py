import socket
import json

import warstaruser

nset = {'Username','Password','Alignment'}


regsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
regsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
regport = 12345

regsock.bind( ('', regport) )

regsock.listen(1)

while True:
	message = '' #Status message 

	clisock, address = regsock.accept()
	print("\nConnection from: " + str( address ))

	alldata = ''
	data = clisock.recv(1)
	alldata = alldata + data
	while len(data) > 0:
		print(data, end=' ')
		data = clisock.recv(1)		
		alldata = alldata + data		
		if data == '}':
			break

	print("\n" + alldata)

	try:
		unjdata = json.loads(alldata)
		if nset != set(unjdata):
			raise warstaruser.UserError("Missing: Username, Password, or Alignment fields")

		message = warstaruser.register(unjdata['Username'], unjdata['Password'], unjdata['Alignment'])
	except warstaruser.UserError as err:
		message = err
	except:
		message = "Failure JSON"

	sendback = json.dumps( {"Result": message} )

	print(sendback)

	clisock.send(sendback)
	clisock.close()
