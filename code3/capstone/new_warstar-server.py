import socket
import select
import json
import sys
import time

import warstaruser

from youngling import Youngling
from acolyte import Acolyte

from padawan import Padawan
from apprentice import Apprentice

from jediknight import JediKnight
from sithmaster import SithMaster

from jedimaster import JediMaster
from sithlord import SithLord

from warstarbattle import resolve
from warstarbattle import calcXP
from warstarbattle import calcXPTie

DEBUG = True


combatdict1 = None
combatdict2 = None

combat1 = None
combat2 = None

player1connsock = None
player1addr = ''
player2connsock = None
player2addr = ''

player1move = ''
player2move = ''

# Player 1
p1LoginDict = None
p1StatusDict = None #Alignment
p1Socket = None
p1Addr = None
p1MoveDict = None


# Player 2
p2LoginDict = None
p2StatusDict = None #Alignment
p2Socket = None
p2Addr = None
p2MoveDict = None

aXP = 0
bXP = 0


def getTier(xp):
    if xp < 200:
        return 1    
    elif xp < 400:
        return 2
    elif xp < 600:
        return 3
    else:
        return 4   


def roundresolve(socka, sockb):
    
    ret = None
    global aXP
    global bXP

    #combat1.regenFP()
    #combat2.regenFP()

    before = (combat1.getCurrentHP(), combat1.getCurrentFP(), combat2.getCurrentHP(), combat2.getCurrentFP())
    rez = resolve(player1move, combat1, p1LoginDict['Username'], player2move, combat2, p2LoginDict['Username'])
    combat1.takeDamage(rez[1])
    combat2.takeDamage(rez[0])


    after = (combat1.getCurrentHP(), combat1.getCurrentFP(), combat2.getCurrentHP(), combat2.getCurrentFP() )

    diff = ( before[0] - after[0],  before[1] - after[1], before[2] - after[2], before[3] - after[3] )
    d = { 'DamageTaken':0, 'EnemyDamageTaken':0, 'FPUsed':0, 'EnemyFPUsed':0, 'EnemyMove':"blahblah", 'XP':0}

    #print "      :HPA, FPA, HPB, FPB)"
    #print "  Before: ", before
    #print "  After : ", after
    #print "  Diff  : ", diff

    print("Character Status")
    print("\t%s: HP: %d, FP: %d\n\t%s: HP: %d, FP: %d\n\n" %(p1LoginDict['Username'], after[0], after[1], p2LoginDict['Username'], after[2], after[3]))

    

    #tie
    if 0 >= after[0] and 0>= after[2]:
        aXP,bXP = calcXPTie( getTier(p1StatusDict['XP']), getTier(p2StatusDict['XP']) )
        ret = (False, "Tie")

    # B wins
    elif 0 >= after[0]: 
        aXP,bXP = calcXP( getTier(p2StatusDict['XP']), getTier(p1StatusDict['XP'])  )
        ret =  (False, "B")

    # A wins
    elif 0 >= after[2]:
        aXP,bXP = calcXP( getTier(p1StatusDict['XP']), getTier(p2StatusDict['XP']) )
        ret = (False, "A")

    else:
        ret = True, "NEVER LOOK HERE" 

    dA = { 'DamageTaken':diff[0], 'EnemyDamageTaken':diff[2], 'FPUsed':diff[1], 'EnemyFPUsed':diff[3], 'EnemyMove':player2move, 'XP':aXP}
    dB = { 'DamageTaken':diff[2], 'EnemyDamageTaken':diff[0], 'FPUsed':diff[3], 'EnemyFPUsed':diff[1], 'EnemyMove':player1move, 'XP':bXP}

    sendback = json.dumps( dA )

    if DEBUG:     
        print("\tSendback A: " + sendback)     
    socka.send(sendback)  

    sendback = json.dumps( dB )     
    if DEBUG:     
       print("\tSendback B: " + sendback)     
    sockb.send(sendback)

    combat1.regenFP()
    combat2.regenFP()

    return ret


       

  


def recvjsonstring(clisock):     
    alldata = ''     
    
    data = clisock.recv(1)     
    alldata = alldata + data     
    #print '\t' + data,     
    while len(data) > 0:         
        data = clisock.recv(1)           
        #print data,         
        alldata = alldata + data                 
        if data == '}':             
            break     
    print("\t" + alldata)      
    return alldata

def recvmove(clisock, address, player):
    ret = None
    tmp = None
    fp = False
    hasmove = False    
    
    if DEBUG:     
       print("Battle Move Connection from: " + str( address ) + " " + str(player))
    alldata = recvjsonstring(clisock) 
    
    if DEBUG:
        print(alldata)    
    try:
        unjdata = json.loads(alldata)        
        if player == 1:    
            player1move = unjdata
        else:    
            player2move = unjdata            
        
        messagedict = {'Result': 'Success'}  
        ret = unjdata['MoveName']       

    except:    
        e = sys.exc_info()[0]
        print(e)    
        messagedict = {'Result': 'Failure', 'Reason': "Failure JSON.loads() "}
        sendback = json.dumps( messagedict )
        if DEBUG:     
            print("\tSendback (%s): %s" %(address, sendback))
        clisock.send(sendback)
        return False, ret   
    
    # Has move
    if unjdata['MoveName'] == 'Attack' :
        hasmove = True

    if unjdata['MoveName'] != 'Attack':
        if unjdata['MoveName'].upper() in combat1.getForceAbilities():
            hasmove = True

    if not hasmove:
        messagedict = {'Result': 'Failure', 'Reason': 'Unknown Move'}
        sendback = json.dumps( messagedict )
        if DEBUG:     
            print("\tSendback (%s): %s" %(address, sendback))
        clisock.send(sendback)
        return False, ret 

    #has FP
    if player == 1 and player1move['MoveName'] != 'Attack':    
        fp = combat1.hasEnoughFP(player1move['MoveName'])
    elif player == 2 and player2move['MoveName'] != 'Attack':      
        fp = combat2.hasEnoughFP(player2move['MoveName'])
    else:
        fp = True
        pass

    if not fp:
        messagedict = {'Result': 'Failure', 'Reason': 'Insufficient FP'} 
        sendback = json.dumps( messagedict )
        if DEBUG:     
            print("\tSendback (%s): %s" %(address, sendback))
        clisock.send(sendback)
        return False, ret 

    sendback = json.dumps( messagedict )
    if DEBUG:     
       print("\tSendback (%s): %s" %(address, sendback))
    clisock.send(sendback)

    return (True, ret);


def recvlogin(clisock, address, player):          
    if DEBUG:     
       print("Login Connection from: " + str( address ))

    ret = None
    tmp = None      

    jsonSuccess = False
    loginSuccess = False       

    messagedict = None
    unjdata = None

    alldata = recvjsonstring(clisock)

    try:        
        unjdata = json.loads(alldata)         
        jsonSuccess = True        
    
    except:    
            
        e = sys.exc_info()[0]           
        errmsg = "json.loads() returned %s" %(e)
        messagedict = {"Result": "Failure", "Reason":errmsg}          
        ret = False

        sendback = json.dumps( messagedict )
        if DEBUG:     
            print("\tSendback: " + sendback)
        clisock.send(sendback)
        return (ret, messagedict, unjdata)
              
    if jsonSuccess:        
        try:             
            messagedict = warstaruser.login(unjdata['Username'], unjdata['Password'])
            ret = True
            
        except:              
            e = sys.exc_info()[0]             
            errmsg = "Login processor returned %s" %(e)             
            messagedict = {"Result": "Failure", "Reason":errmsg}      
                
    sendback = json.dumps( messagedict )
    if DEBUG:     
       print("\tSendback: " + sendback)
    clisock.send(sendback)

    return (ret, messagedict, unjdata)


def makeForceUser(thedict):
    #Tier 1: 0-199 XP
    #Tier 2: 200-399 XP
    #Tier 3: 400-799 XP
    #Tier 4: 800+ XP

    align = thedict['Alignment']
    xp = thedict['XP']

    if xp < 200:
        if align == 'Jedi':    
            return Youngling(1, (1,3,0), 10, 20, 3)
        elif align == 'Sith':    
            return Acolyte(1, (1,3,0), 10, 20, 3)
        else:    
            print("WTF1")    
            sys.exit(1)
    elif xp < 400:
        if align == 'Jedi':    
            return Padawan(3, (1,6,1), 12, 30, 4)
        elif align == 'Sith':    
            return Apprentice(3, (1,6,3), 12, 25, 4)
        else:    
            print("WTF2")    
            sys.exit(1)
    elif xp < 600:
        if align == 'Jedi':    
            return JediKnight(6, (2,6,2), 18, 50, 6)
        elif align == 'Sith':    
            return SithMaster(6, (2,6,6), 15, 40, 6)
        else:    
            print("WTF2")    
            sys.exit(1)
    else:
        if align == 'Jedi':    
            return JediMaster(12, (3,6,3), 22, 75, 8)
        elif align == 'Sith':    
            return SithLord(12, (3,6,8), 18, 60, 8)
        else:    
            print("WTF2")    
            sys.exit(1)    

p1port = 4242
p1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
p1.setblocking(0)
p1.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
p1.bind( ('', p1port) )
p1.listen(1)

p2port = 1337
p2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
p2.setblocking(0)
p2.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
p2.bind( ('', p2port) ) 
p2.listen(1)


# Handle Logins
while True:
    print("@@@@@@@@@ LOGIN @@@@@@@@@@@@@@@@")

    p1login = False
    p2login = False

    inputs = []    
    if not p1login:
        inputs.append(p1)
    if not p2login:
        inputs.append(p2)

    # Sockets to which we expect to read
    inputs = [ p1, p2 ]
    # Sockets to which we expect to write
    outputs = [ ]

    while True:
        readable, writable, exceptional = select.select(inputs, outputs, inputs)    
        for s in readable:        
            #print '----------------'    
            #print "p1 login? " + str(p1login)    
            #print "p2 login? " + str(p2login)    
            #print "S == p1? " + str(s==p1)    
            #print "S == p2? " + str(s==p2)        
            if s == p1 and not p1login:  
                print("Player 1")      
                clisock1, address = p1.accept()        
                q = recvlogin(clisock1, address, 1)
                p1StatusDict = q[1]
                p1LoginDict = q[2]
        
                if q[0]:            
                    p1login = True            
                    player1connsock = clisock1            
                    inputs.remove(p1)            
                    combatdict1 = q[1]            
                    combat1 = makeForceUser(p1StatusDict)           
                    player1addr = address        
            if s == p2 and not p2login:
                print("Player 2")           
                clisock2, address = p2.accept()
                q = recvlogin(clisock2, address, 2)
                p2StatusDict = q[1]
                p2LoginDict = q[2]
           
                if q[0]:            
                    p2login = True            
                    player2connsock = clisock2            
                    combatdict2 = q[1]            
                    combat2 = makeForceUser(p2StatusDict)            
                    player2addr = address 

        if p1login and p2login:            
            print("BOTH LOGIN SUCCESS")
            break       



    if p1login and p2login:   

        pl1d = {'Name':p1LoginDict['Username'], 'XP':p1StatusDict['XP'],  'Alignment':p1StatusDict['Alignment'] }
        pl2d = {'Name':p2LoginDict['Username'], 'XP':p2StatusDict['XP'],  'Alignment':p2StatusDict['Alignment'] }

        pl1 = json.dumps( pl1d )
        pl2 = json.dumps( pl2d )


        player1connsock.send('\nHello %s. 2 players connected.' %( p1LoginDict['Username'] ))
        player1connsock.send(pl1)

        if DEBUG:
            print("Sendback: ", pl1)

        #player1connsock.flush()            
        player2connsock.send('\nHello %s. 2 players connected.' %( p2LoginDict['Username'] ))
        player2connsock.send(pl2)
        if DEBUG:
            print("Sendback: ", pl2)
        #player2connsock.flush()
        break

#Pass name



time.sleep(1)

roundcnt = 1
finish = False
finstr = 'OOPS'
winnar = 'QWERTY'

while not finish:

    print("@@@@@@@@@ Battle @@@@@@@@@@@@@@@@")

    p1move = False
    p2move = False    
    inputs = []

    while True:


        # Sockets to which we expect to read
        if not p1move:
            inputs.append( player1connsock )
        if not p2move:
            inputs.append( player2connsock )

        # Sockets to which we expect to write
        outputs = []

        readable, writable, exceptional = select.select(inputs, outputs, inputs) 

        #print '----while loop'    
        for s in readable:    
            #print inputs        
            #print '----for loop'    
            #print "p1 move? " + str(p1move)    
            #print "p2 move? " + str(p2move)    
            #print "S == p1? " + str(s==player1connsock)    
            #print "S == p2? " + str(s==player2connsock)  
            #raw_input('Press Enter key')      
            if s == player1connsock and not p1move:        
                if DEBUG:     
                    print('%s is selecting a move...' %( p1LoginDict['Username'] ))

                q = recvmove(player1connsock, player1addr, 1)               
                if q[0]:            
                    p1move = True
                    player1move = q[1]            
                    inputs.remove(player1connsock)
                else:
                    print("Move not accepted! Retry")  

            if s == player2connsock and not p2move:        
                if DEBUG:     
                    print('%s is selecting a move...' %( p2LoginDict['Username'] )) 

                q = recvmove(player2connsock, player2addr, 2)       
                if q[0]:            
                    p2move = True
                    player2move = q[1]            
                    inputs.remove(player2connsock) 
                else:
                    print("Move not accepted! Retry")       
            
            if p1move and p2move:        
                print("ROUND %d\t===============================================" %(roundcnt))        
                end = roundresolve(player1connsock, player2connsock) 
                #print '@@@@@@'
                #print end           
                p1move = False        
                p2move = False        
                roundcnt = roundcnt + 1 

                if not end[0]:
                    if DEBUG:     
                        print("BATTLE END")
                    finish = True
                    winnar = end[1]

        if finish:

            finstr = '\nBattle Finished. %s wins' %(winnar)

            #player1connsock.send(finstr )
            player1connsock.close()
            #player1connsock.flush()            
            #player2connsock.send(finstr )
            player2connsock.close()
            #player2connsock.flush()
            print(finstr)

            warstaruser.updateXP(p1LoginDict['Username'], aXP)
            warstaruser.updateXP(p2LoginDict['Username'], bXP)
            break   
#BATTLE!