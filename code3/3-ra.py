from raw_socket_helper import RawSocket_IPv6

interfaceName = "ens33"
#interfaceName = "enp0s3"
raw_socket = RawSocket_IPv6(interfaceName)

srcmac = b'\x08\x00\x27\x37\xad\x8f' # real hardware address of interface
srcip = b'\xfe\x80\x00\x00\x00\x00\x00\x00\x56\x49\xf9\x8e\x15\x0d\xcc\xc0' # LINK-LOCAL ADDRESS

dstmac = b'\x33\x33\x00\x00\x00\x01' # layer 2 IPv6Mcast
dstip = b'\xff\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01' # ff02::1 all nodes LINK-LOCAL scope

# icmpv6 option: SOURCE LINK LAYER OPTION
icmpv6opt2 = b'\x01' #type
icmpv6opt2 += b'\x01' #len
icmpv6opt2 += srcmac # my mac addr

# icmpv6 option: ROUTER ADVERTISEMENT PREFIX OPTION
icmpv6opt =  b'\x03'     # type
icmpv6opt += b'\x04'     # len
icmpv6opt += b'\x40'     # prefix len (64)
icmpv6opt += b'\xe0'     # flags: A L 6bit reserved1
icmpv6opt += b'\x00\x00\x1c\x20'     # valid lifetime
icmpv6opt += b'\x00\x00\x1c\x20'     # preferred lifetime
icmpv6opt += b'\x00\x00\x00\x00'     # reserved2
icmpv6opt += b'\x00\x06\x00\x07\x00\x07\x07\x0b\x00\x00\x00\x00\x00\x00\x00\x00' # 5:5:5:B::

# icmp v6
icmpv6hdr =  b'\x86'         # type
icmpv6hdr += b'\x00'         # code
icmpv6hdr += b'\x00\x00'     # checksum calculated by helper code
icmpv6hdr += b'\xff'         # current hop limit
icmpv6hdr += b'\x80'         # M O reserved
#icmpv6hdr += '\x0e\x10'     # lifetime
icmpv6hdr += b'\x00\x00'     # lifetime
icmpv6hdr += b'\x00\x00\xff\xff' # reachable
icmpv6hdr += b'\x00\x00\xff\xff' # retrans
icmpv6 = icmpv6hdr + icmpv6opt2 + icmpv6opt

# IPv6 Header (aka Ethernet Payload)
ipv6hdr = b'\x60\x00\x00\x00'    # ver, traffic class, flow label (odd byte alignment if broken apart)
ipv6hdr += b'\x00\x38'           # payload sz
ipv6hdr += b'\x3a'               # next header
ipv6hdr += b'\xff'               # hop limit
ipv6hdr += srcip
ipv6hdr += dstip
ipv6hdr += icmpv6

# Ethernet Header
dst = dstmac
src = srcmac
typ = b'\x86\xdd'    # 0x0800 is IPv4, 0x0806 is ARP, 0x86dd is IPv6

etherhdr = dst + src + typ

frame = etherhdr + ipv6hdr

for i in range(3):
    raw_socket.send_chksum(frame)
