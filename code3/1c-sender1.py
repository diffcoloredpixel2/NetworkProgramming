import socket   				# Import socket module
import json
import time

udpsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  	# Create a socket object
udpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

myport = 9999                  # Port for your server
myhost = '127.0.0.1'            # Your IP to listen on
b = (myhost, myport)            # The Python function bind() requires a tuple of host,port 
udpsock.bind(b)                # Bind to the port


destport = 6789  				# Port for your server
desthost = '127.0.0.1'  		# IP of the server
dest = (desthost, destport)		# The Python function sendto() requires a tuple of host,port 

msg = "The quick brown fox jumped over the lazy dogs"

while True:
    udpsock.sendto(msg, dest)	# send the data back
    print('Sent %s to %s\n' %(msg, dest))
    time.sleep(2)


#data, clientip = udpsock.recvfrom(256)	# recv data
#print 'Sent %s to %s\nrecv: %s from %s\n' %(msg, dest, data, clientip)