import socket
import struct

class err(Exception):
    def __str__(self):
        return b"Packets should be 16 bit"


class err2(Exception):
    def __init__(self, msg):
        super(Exception, self).__init__(msg)
        self.msg = msg

    def __str__(self):
        return b"Cannot calculate checksum IP proto 0x%02x" % self.msg


class err3(Exception):
    def __init__(self, msg):
        super(Exception, self).__init__(msg)
        self.msg = msg

    def __str__(self):
        return b"Non-IP ethertype 0x%04x" % self.msg


class RawSocket(object):
    """Helper functions for Network Programming"""
    def __init__(self, interface):
        self.interface = interface

    def send(self, frame):
        """
        Send raw ethernet frame.
        """
        class err(Exception):
            def __str__(self):
                return b"Incorrect ethernet frame"

        if len(frame) < 14:
            raise err

        s = socket.socket(socket.PF_PACKET, socket.SOCK_RAW)
        s.bind((self.interface, 0))
        try:
            s.send(frame)
        except:
            raise ValueError(b"Try adding a checksum")
        finally:
            s.close()

    def send_chksum(self, frame):
        """
        Perform IP, ICMP, UDP, and/or TCP checksum
        calculations and send raw frame.
        """
        ip_proto = 0
        f_type = 0

        def checksum(data):
            if len(data) % 2:       # packets have to end on 16 bit boundaries...
                raise err
            total = 0
            for i in range(0, int(len(data)/2)):
                total += struct.unpack(">H", data[i*2:(i+1)*2])[0]
            total = (((total >> 16) + total) & 0xffff) ^ 0xffff
            print("checksum: ", total)
            return total

        try:
            f_type = struct.unpack(">H", frame[12:14])[0]
        except:
            raise err
        if f_type != 0x0800 and f_type != 0x86dd:
            raise err3(f_type)

        try:
            ip_pkt = frame[14:]
            ip_header_len = (ip_pkt[0] & 0x0f) * 4
            ip_chksum = checksum(ip_pkt[:ip_header_len])
            ip_pkt = ip_pkt[:10] + struct.pack(">H", ip_chksum) + ip_pkt[12:]
            ip_proto = ip_pkt[9]
            ip_total_len = struct.unpack(">H", ip_pkt[2:4])[0]
        except:
            raise err

        if ip_proto == 1:
            try:
                icmp_sum = checksum(ip_pkt[ip_header_len:ip_total_len])
                ip_pkt = ip_pkt[:ip_header_len+2] + struct.pack(">H", icmp_sum) + \
                        ip_pkt[ip_header_len+4:]
            except:
                raise err

        elif ip_proto == 6 or ip_proto == 17:
            try:
                pseudo_hdr = ip_pkt[12:20] + b"\x00" + struct.pack('b',ip_pkt[9]) + \
                            struct.pack(">H", ip_total_len - ip_header_len)
                total = checksum(pseudo_hdr + ip_pkt[ip_header_len:ip_total_len])
                sum_off = ip_header_len + {6:16, 17:6}[ip_proto]
                ip_pkt = ip_pkt[:sum_off] + struct.pack(">H", total) + ip_pkt[sum_off+2:]
            except:
                raise err
        else:
            raise err2(ip_proto)

        self.send(frame[:14] + ip_pkt)


class RawSocket_IPv6(RawSocket):
    """Helper functions for Network Programming"""
    def __init__(self, interface):
        super(RawSocket_IPv6, self).__init__(interface)
    
    def send_chksum(self, frame):
        """
        Perform IP, ICMP, UDP, and/or TCP checksum
        calculations and send raw frame.
        """
        def checksum(data):
            total = 0
            for i in range(0, int(len(data)/2)):
                total += struct.unpack(">H", data[i*2:(i+1)*2])[0]
            total = (((total >> 16) + total) & 0xffff) ^ 0xffff

            return total

        try:
            f_type = struct.unpack(">H", frame[12:14])[0]
        except:
            raise err
        if f_type != 0x0800 and f_type != 0x86dd:
            raise err3(f_type)

        try:
            ip_pkt = frame[14:]
            ip_class = struct.unpack(">H", ip_pkt[0:2])[0] & 0x0ff0
            ip_label = struct.unpack(">L", ip_pkt[0:4])[0] & 0x0fffff
            ip_payload = struct.unpack(">H", ip_pkt[4:6])[0]
            if ip_payload == 0:
                # calculate payload:
                ip_payload = len(ip_pkt[40:])
                ip_pkt = ip_pkt[:4] + struct.pack(">H", ip_payload) + ip_pkt[6:]
            ip_next_hdr = (ip_pkt[6])
            ip_hop_lim = (ip_pkt[7])

            # Upper-Layer Packet Length is 32-bit in pseudo header vice 16 bit in IPv6 header
            # If upper layer protocol includes length field, use that, if not, take payload length from ipv6 header, minus length of any extension headers

        except:
            raise err

        if ip_next_hdr == 58:
            try:
                # Calculate payload without extension headers:
                ip_pseudo_hdr = ip_pkt[8:24] + ip_pkt[24:40] + b"\x00\x00" + struct.pack(">H", ip_payload) + b"\x00\x00\x00" + struct.pack('b', ip_pkt[6]) #str(ip_pkt[6]).encode('utf-8')
                icmp_sum = checksum(ip_pseudo_hdr+ip_pkt[40:])
                ip_pkt = ip_pkt[:40+2] + struct.pack(">H", icmp_sum) + ip_pkt[40+4:]
            except:
                raise err
        elif ip_next_hdr == 17:
            # UDP
            try:
                udp_payload = struct.unpack(">H", ip_pkt[40+4:40+6])
                ip_pseudo_hdr = ip_pkt[8:24] + ip_pkt[24:40] + b"\x00\x00" + struct.pack(">H", udp_payload[0]) + b"\x00\x00\x00" + struck.pack('b', ip_pkt[6])
                total = checksum(ip_pseudo_hdr + ip_pkt[40:])
                sumoff = 40 + {6:16, 17:6}[ip_next_hdr]
                udp_len = len(ip_pkt[40:])-1
                ip_pkt = ip_pkt[:sumoff] + struct.pack(">H", total) + ip_pkt[sumoff+2:]
                # length auto-calc:
                ip_pkt = ip_pkt[:44] + struct.pack(">H", udp_len) + ip_pkt[46:]
            except:
                raise err
        elif ip_next_hdr == 6:
            # TCP
            try:
                ip_pseudo_hdr = ip_pkt[8:24] + ip_pkt[24:40] + b"\x00\x00" + struct.pack(">H", ip_payload) + b"\x00\x00\x00" + struct.pack('b', ip_pkt[6])
                total = checksum(ip_pseudo_hdr + ip_pkt[40:])
                sumoff = 40 + {6:16, 17:6}[ip_next_hdr]
                ip_pkt = ip_pkt[:sumoff] + struct.pack(">H", total) + ip_pkt[sumoff+2:]
            except:
                raise err
        else:
            raise err2(ip_next_hdr)

        self.send(frame[:14] + ip_pkt)
