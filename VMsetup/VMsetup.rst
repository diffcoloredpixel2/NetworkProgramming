:title: Introduction to Network Programming
:data-transition-duration: 1500
:css: ../networking.css

=======================================================
How to get your VM ready for networking programming...
=======================================================
Hopefully
-------------------------------------------------------
 
:: 

    ...

#######################

====================================
VMWare
====================================

Click Edit > Virtual Network Editor

.. image:: VMWare.png


#######################

====================================
Virtual Network Editor
====================================

Click 'Change Settings' at the bottom right

.. image:: VirtualNetworkEditor_User.png


#######################

====================================
VMWare
====================================

Select VMNet 0: Bridged

Ensure Bridged radio button is set

Click Advanced Settings

If you do not have VMNet 0, click "Restore Defaults"

.. image:: VirtualNetworkEditor_Admin.png


#######################

====================================
VMWare
====================================

Select your ethernet adapter

Unselect everything else

.. image:: VMware_AutomaticBridgingSettings_List.png

#######################

====================================
VMWare
====================================

Set your VM to Bridged Mode while it is shutdown

.. image:: VMSettings.png


