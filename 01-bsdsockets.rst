:title: Introduction to Network Programming
:data-transition-duration: 1500
:css: networking.css

====================================
Concepts Overview  
====================================

- Sockets

- BSD Socket API

- TCP Client Call Sequence

- TCP Server Call Sequence

- TCP and sockets

- UDP Client Call Sequence

- UDP Server Call Sequence

- UDP and sockets


#######################

====================================
Sockets Overview
====================================

Sockets allow for Inter-Process Communication (IPC) 

We will be using the BSD Socket API for networking

Knowing BSD sockets will provide a foundation to do networking in almost any language/environment

  - Even Windows' Winsock library still follows the call pattern, even if Microsoft makes their function calls take a dozen more parameters

#######################

====================================
BSD Sockets
====================================

In order to transfer data between hosts, you need to make a socket. A socket consists of an address family and a layer 4 protocol. 

To make a socket, you must to answer 2 questions:

  - What type of address will I use to talk with the remote host?
  - How do I want to handle data transmission?


#######################

====================================
BSD Sockets - Address Family
====================================

The type of address determines the "address family". We typically use IPv4 or IPv6, though many others exist. For IPv4 and IPv6 we will use the following constants defined in Python's socket module: 

  - AF_INET
  - AF_INET6


#######################

====================================
BSD Sockets - Data Transmission
====================================

The data transmission determines your layer 4 protocol. 

Typically you will use UDP under the following conditions:
  - You do not need a reply
  - You do not need error handling OR cannot tolerate the overhead of implementing it
  - The data is a small request that elicits a small response (e.g. DNS lookup)
  - There is a large amount of data, but data loss is acceptable/expected (e.g. streaming video)


TCP is best used under the following conditions:
  - Reliably transferring large amounts of data
  - The transmission must handle errors 
  - You have no idea what else to pick

Python defines these as constants in the socket module
  - SOCK_DGRAM (UDP)
  - SOCK_STREAM (TCP)
  - SOCK_RAW

Using a TCP or UDP socket will cause the OS to handle layers 2-4 for you. If you neeed control of these layers a RAW socket is required, however you are responsible for properly formatting each layer   

#######################


====================================
Ports
====================================

Ports are how processes get access to the network.

Port numbers < 1024 are considered "privleged" ports
  
  - Processes must have root/admin privileges to bind to these ports

  - They are typically reserved for servers. IANA maintains the list of well known ports (see references)

Ports >= 1024 can be used by anyone

If you do not bind to a port as a TCP client/UDP sender, you will get a random high number port automatically assigned to your process. This is called an "ephemeral port" 

#######################



====================================
TCP Client Order of Operations
====================================

Use PyDocs for more information on these function calls and details on their parameters/return values

socket( Address Family, Layer 4 Protocol ) 
  - Get a socket descriptor
  - Returns a socket object

bind( (Local IP, Local Port) ) 
  - Optional for Client
  - Specify SOURCE port instead of using ephemeral
  - Must use ip address assigned to the host that is in the socket's Address Family 
  - May use '' to mean any address assigned to the host

connect( (Remote IP,Remote PORT) ) 
  - Connect to destination IP/PORT

send( data ) / recv( bufsize ) 
  - Performs Data transfer
  - Send returns number of bytes sent
  - Recv returns a string of data received (up to bufsize)

close() 
  - Close the socket (which is a file descriptor)
  - Kills any connection and stops data transfer


#######################

====================================
TCP Server Order of Operations
====================================

socket( Address Family, Layer 4 Protocol ) 
  - Get a socket descriptor
  - Returns a socket object

bind( (Local IP, Local Port) ) 
  - Mandatory for Server
  - Must specify the port you intend to receive traffic on. 
  - Must use ip address assigned to the host that is in the socket's Address Family 
  - May use '' to mean any address assigned to the host

listen( queue_size ) 
  - Wait for a client to connect on the port
  - Up to queue_size TCP connections will be accepted

accept() 
  - Establishes a connection with a connected host
  - Returns a tuple containing the NEW socket to communicate with a single client, and a tuple of remote ip/port (more on this on the next slide)

send( data ) / recv( bufsize ) 
  - Performs Data transfer
  - Send returns number of bytes sent
  - Recv returns a string of data received (up to bufsize)

close() 
  - Close the socket (which is a file descriptor)
  - Kills any connection and stops data transfer

#######################

====================================
Tale of two sockets
====================================

TCP Server code will deal with TWO (or more) sockets

  - The first socket is created by socket() and is used to LISTEN
  - The other sockets are returned by accept() after a client connects to the listening socket. This new socket is used for data transfer to that specific client


Keep in mind

  - Calls to accept() will NOT alter the LISTENING socket
  - By default, accept() will block until a connection is received

#######################

====================================
Connections
====================================

A CONNECTION is a unique combination of
  - LOCAL IP
  - LOCAL Port
  - REMOTE IP
  - REMOTE Port

If any one of these changes, it must be a different connection

#######################

====================================
UDP Order of Operations
====================================

socket( Address Family, Layer 4 Protocol ) 
  - Get a socket descriptor
  - Returns a socket object

bind( (Local IP, Local Port) ) 
  - Mandatory for a receiver, optional for a sender
  - Must specify the port you intend to receive traffic on. 
  - Must use ip address assigned to the host that is in the socket's Address Family 
  - May use '' to mean any address assigned to the host
 
sendto( data, (Remote IP, Remote Port) ) / recvfrom( bufsize ) 
  - Performs Data transfer
  - sendto returns number of bytes sent
  - recvfrom returns a string of data received (up to bufsize) AND a tuple of (Remote IP, Remote Port) indicating who sent the data

close() 
  - Close the socket (which is a file descriptor)
  - Kills any connection and stops data transfer


#######################

====================================
UDP: Client vs Server
====================================

UDP is connectionless

A UDP socket exists independently of any specific remote IP/PORT
  - Both sending and receiving data occur on the same socket, even to/from different remote hosts

You must track the source of incoming data and destination of outgoing data
  - This info must be passed to sendto() and is returned by recvfrom()

Unlike TCP, there is not a clean delinieation between client and server based on the function calls. 

It's usually easier to think of them as "Senders" and "Recievers"
  - If you want to recieve traffic, you need bind to a port. 

#######################

====================================
sendto() and recvfrom()
====================================

recvfrom() returns a tuple of TWO values, the data and a tuple of remote ip/port.

sendto() takes a tuple TWO arguments, data and a tuple of the remote ip/port 

Use the clientIP from recvfrom() in the call to sendto() to return data to that host

You may use multiple assignment here as well

::
  
  data, remote = sock.recvfrom()

#######################


====================================
Multiple Assignment
====================================

::

    ret = sock.accept()
    ret[0] # The socket
    ret[1] # tuple of remote ip/port

    accept_socket, remote = sock.accept()

::

    ret = sock.recvfrom()
    ret[0] # The data
    ret[1] # tuple of remote ip/port

    data, remote = sock.recvfrom()


#######################

====================================
setsockopt() and getsockopt()
====================================

Socket options allow you to specify some advanced networking features. These features may be required for special networking cases or just make your life easier. 

getsockopt() and setsockopt() allow you check or modify socket options. Socket options are scattered everywhere in documentation. For a grouping of common ones, see the refs. 

Socket options should be set IMMEDIATELY after creating the socket. 

setsockopt(LEVEL, OPTION NAME, VALUE)
  
  - LEVEL can be the entire socket (socket.SOL_SOCKET) or a just specific protocol (socket.IPPROTO_IPV6) 

  - OPTIONs are determined by the LEVEL selected. Each level has different options you can set

  - VALUES are assigned to the specified OPTION NAME


Examples:

mysock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

mysock.setsockopt(socket.IPPROTO_IPV6, socket.JOIN_GROUP, group)



#######################


====================================
Struct Module
====================================

Struct allows you to pack values into specified data types/sizes and endianess. Packed data is represented by a string of hex bytes. 

Struct will also unpack data from the hex string and provide you a tuple of the values.

Packing is used to prepare structured binary data such as a protocol header or a message format. This data can then be referenced like a struct in C, or sent across the wire. 

It uses a format string and variable arguments (like printf in C). A table indicating the values for the format string can be found in the PyDocs (see refs)

Neither pack() nor unpack() alter any variables passed into their parameters


::

  >>> from struct import *
  >>> pack('hhl', 1, 2, 3)
  '\x00\x01\x00\x02\x00\x00\x00\x03'
  >>> unpack('hhl', '\x00\x01\x00\x02\x00\x00\x00\x03')
  (1, 2, 3)

#######################


====================================
JSON Module
====================================

JSON = JavaScript Object Notation. It's a series of key-value pairs. The key-value pairs may be nested.

json.dumps() creates a JSON string from the data passed in. It looks like a Python dictionary with quotes around it

::

  >>> import json
  >>> data = {'foo':1, 'bar':'qwerty'}
  >>> json.dumps(data)
  '{"foo": 1, "bar": "qwerty"}'
  >>> data
  {'foo': 1, 'bar': 'qwerty'}
  >>> 


json.loads() takes a JSON String and  makes it onto a dictionary

::

  >>> jsonstr = json.dumps(data)
  >>> type(json.loads(jsonstr))
  <type 'dict'>

#######################


====================================
References
====================================

PyDocs: 

  https://docs.python.org/2/library/socket.html

  https://docs.python.org/2.7/library/json.html

  https://docs.python.org/2/library/struct.html


Man Pages:

  http://man7.org/linux/man-pages/man7/socket.7.html

  http://man7.org/linux/man-pages/man7/ip.7.html

  http://man7.org/linux/man-pages/man7/ipv6.7.html


Other:

  http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml

  https://notes.shichao.io/unp/ch7/


#######################


====================================
Python 3 Special Note
====================================

Converting from bytes to str and back

::

  bstr = b'qwerty'
  type (bstr)
      <class 'bytes'>

  str = bstr.decode('utf-8')
  type(str)
      <class 'str'>

  bstr2 = str.encode('utf-8')
  type(bstr2)
      <class 'bytes'>

You may also use strings like normal, and call encode()

::

  mystr = 'qwerty'
  mystr.encode('UTF-8')


#######################

====================================
CLASS DEMO
====================================

Please open up both the slides and the Python documentation. We will create a simple TCP server and TCP client as a class. 

I will type what you say blindly into the interpreter (even if wrong). As a class, we will correct any errors that occur.  

Feel free to follow along.

NOTE: This is for illustrative purposes only. When doing labs, do NOT use the interpreter. Instead, write a python file and run it from the command line. 

#######################

====================================
LABS
====================================

The following slides contain several labs for you to do. If you finish early, I have included bonus labs to provide an extra challenge. Solving these will serve you well in this line of work.

For these labs, make a new python file for each client and server. Use the terminal to run your files with python or python3. 

You may arbitrarily choose the port numbers for the below labs

Write your code neatly and with comments


#######################

====================================
LABS - Debugging
====================================

If you have a client and server that is not working properly, use netcat to help you debug one side at a time.

Start a netcat listener, and connect to it with your client. It will print anything your client sends to the terminal

Start your server code, then use netcat to connect to it. Whatever you type into the terminal will be sent to your server. 

Either way, you can see exactly what what is going on. 

For UDP, use the UDP flag.


#######################

====================================
Socket options
====================================

If you encounter an error "Address in use", set the following socket option on your sockets (both client and server). You may have to wait out the intial timer (or change your ports)

mysock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)


#######################

====================================
LAB 1A
====================================

Write a TCP server that recieves a string, reverses order of the words, and sends it back to the client. 

Write a TCP client sends a string then prints the response


#######################

====================================
LAB 1B
====================================

Write a UDP sender that takes a dictionary, turns it into a JSON string, and sends it to a listener. 

Write the UDP reciever to recieve the JSON string and turn it back into a dictionary. 

Validate with print type(jsontodict) and ensuring it prints <type 'dict'> 

#######################


====================================
LAB 1C
====================================

Write a UDP reciever that recieves a string, and orders the words from longest to shortest in a new string.

That new string should be sent to the remote port+1. The remote port is part of the tuple returned by recvfrom()

Write a UDP sender that sends the initial string, and receives the response from the reciever above

Hint: This is a bit of a brain bender and requires piecing together a couple things we covered.


#######################


====================================
LAB 1D
====================================

NOTE: This lab has a Q&A aspect to it. 

Pack the values (1, 2, -3, -4) in NETWORK byte order (see table 1 in PyDocs for struct) as the following data types (unsigned short, unsigned int, signed short, signed int)

::  

  1 as an unsigned short 
  2 as an unsigned int
  -3 as a signed short 
  -4 as a signed int

Take that packed variable, unpack it using little endian, and print the result. Then unpack it using big endian and print the result.

Q1) Which unpacked are the same as the initial values? 

Q2) Why are they the same? 

Q3) How can I be sure that the data I send is properly recieved on any endian machine?


#######################


====================================
LAB 1E
====================================

The below is the header for an AMF message. You do not need to implement a valid AMF message, or even understand what AMF is. This lab is to practice making a struct.pack/unpack format string based on a real world specification 

+---------------------+-----------------------------+-------------+
| Field Name          | Length                      | Type        |
+=====================+=============================+=============+
| header-name-length  | 16 bits                     | uimsbf      |
+---------------------+-----------------------------+-------------+
| header-name-string  | header-name-length*8 bits   | UTF-8       |
+---------------------+-----------------------------+-------------+
| must-understand     | 8 bits                      | uimsbf      |
+---------------------+-----------------------------+-------------+
| header-length       | 32 bits                     | simsbf      |
+---------------------+-----------------------------+-------------+
| AMF0 or AMF3        | header-length*8 bits        | free form   |
+---------------------+-----------------------------+-------------+

Create a format string that represents the above chart, then supply appropriate values for that string. 

Some of the lengths are **variable widths**. You may arbitrarily choose a width for these fields. I recommend small numbers because future fields will reference a prior field's width (including widths you may have picked). Ensure that you do this multiplication correctly

The type field in the header contains a mnemonic, use it to determine :

  uimsbg = Unsigned Integer, transmitted Most Significant Bit First
  
  simsbg = Signed Integer, transmitted Most Significant Bit First

::

  For example

  header-name-length is a 16 bit field which equals 2 bytes. The Type is uimsbf, which means its unsigned. I use H in my format string because it represents an unsigned integer of 2 bytes. I arbitrarily give it a value of 3

  header-name-string is the actual string. It's width depends on the value specified previously (3). UTF-8 is essentially an ascii string, so I supply a value of 'lab' or 'l' 'a' 'b which is string of length 3 or 3 individual characters. 


#######################

====================================
Concepts Review  
====================================

Sockets allow for IPC

BSD Socket API is used for networking

Function calls to make a TCP Server
  - Socket, Bind, Listen, Accept, Send/Recv, Close

Function calls to make a TCP Client
  - Socket, Connect, Send/Recv, Close

Function calls to make a UDP Server are identical to UDP Clients
  - Socket, Bind, SendTo/RecvFrom, Close

Byte order matters, NETWORK byte order is big endian, use struct module

JSON is string based. Python dictionaries convineitly can be used to store the data


====================================
Homework: ARP RFC
====================================

Let's look at RFC 826 for an example of ARP.
  -  https://tools.ietf.org/html/rfc826

Read the following sections

  - Motivations
  - Definitions
  - Packet Format
  - Packet Generation
  - Packet reception

Identify the ethernet header fields and their widths

Identify the ethernet payload (This ancient RFC calls it "packet data") fields and their widths

Why does ARP allow for certain fields to be variable width? How can you tell how long that field is?

Identify the values that should be in each of those fields you have identified

Explain which values change on between an ARP request and an ARP reply, and why they change.
  - It is more than just "fill in the address"
