import socket   				# Import socket module

connsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  	# Create a socket object
connsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

destport = 12345  				# Port of the server
desthost = '127.0.0.1'  		# IP of the server
dest = (desthost, destport)		# The Python function connect() requires a tuple of host,port 

connsock.connect(dest)			# Connect to server

msg = "Hello is anybody there?"
connsock.send(msg)				# Send message

data = connsock.recv(256)		# Recv data, though not guaranteed from server

print 'Sent \'%s\' to %s, recv: \'%s\'' %(msg, dest, data)

connsock.close()  				# Close the connection