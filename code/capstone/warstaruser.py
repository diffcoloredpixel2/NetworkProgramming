userdb = 'data/user.db'
keyname = ['alignment', 'xp']

class UserError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def usercheck(fh, data):
    fdata = fh.readlines()
    for i in xrange(len(fdata)):
        if data.split(':')[0] == fdata[i].split(':')[0]:
            fh.seek(0, 0)
            raise UserError("Username already exists")

    fh.seek(0, 0)

def register(username, password, alignment):
	try:
		fd = open(userdb, 'a+')

		line =  "%s:%s:%s:%d\n" %(username, password, alignment.title(), 0)
		usercheck(fd, line)
		fd.write(line)
		fd.close()

		print "\tRegistered: " + line
		return "Success"
	except UserError as err:
		print err
		return err
	except:
		print "Register error: (%s, %s, %s, %d)"  %(username, password, alignment, 0)
		return "Registration Failed"

def login(username, password):
	fd = open(userdb, 'r')
	lines = fd.readlines()

	for line in lines:
		#find the user
		if line.startswith(username):
			explode = line.split(':')
			#validate password
			if explode[1] == password :
				return { 'Result': 'Success', 'Alignment': explode[2], 'XP': float(explode[3]) }
	return {'Result': 'Invalid User or Password'}

def updateXP(username, xp):

	fd = open(userdb, 'r')

	lines = fd.readlines()
	fd.close()

	cnt = 0
	for line in lines:
		if line.startswith(username):
			explode = line.split(':')
			explode[3] = str(xp)
			lines[cnt] = ':'.join(explode) + '\n'

		cnt += 1

	fd2 = open(userdb, 'w')
	for l in lines:
		fd2.write(l)
	fd2.close()



if __name__ == '__main__':
	register("matt","test")
