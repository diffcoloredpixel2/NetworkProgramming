class ForceUser:
    #def __init__(self):
    def __init__(self, toHit, dmg, defense, hp, fp, abilityList = {}):
        self.toHitBonus = toHit
        self.damage = dmg
        self.defense = defense
        self.maxHP = hp
        self.currentHP = hp
        self.maxFP = fp
        self.currentFP = fp
        self.ForceAbilities = abilityList

    def takeDamage(self, dmg):
        #if dmg < 0:
        #    raise ValueError("FAIL. Can't take negative dmg")

        self.currentHP -=dmg

    def heal(self, hp):
        self.currentHP += hp

        if self.currentHP > self.maxHP:
            self.currentHP = self.maxHP

    def startCombat(self):
        self.currentFP = self.maxFP
        self.currentHP = self.maxHP

    def getCurrentHP(self):
        return self.currentHP

    def getCurrentFP(self):
        return self.currentFP

    def getToHitBonus(self):
        return self.toHitBonus

    def getDefense(self):
        return self.defense

    def getDamage(self):
        return self.damage

    def getForceAbilities(self):
        return self.ForceAbilities

    """
    def canUseForceAbility(self, AbilityName):
        for ability in self.ForceAbilities:
            if ability[0].upper() == AbilityName.upper():
                if self.getCurrentFP() >= ability[1]:
                    return True
                else:
                    raise ValueError("not enough FP", AbilityName)

        raise ValueError("FAIL. No ability called", AbilityName)
    """

    def hasEnoughFP(self, AbilityName):
        d = self.getForceAbilities()
        power = d[AbilityName.upper()]

        if self.getCurrentFP() < power[0]:
            return False 
        return True

    def abilityFPCost(self, AbilityName):
        d = self.getForceAbilities()
        power = d[AbilityName.upper()]

        return power[0]

    def useForceAbility(self, AbilityName):
        d = self.getForceAbilities()
        power = d[AbilityName.upper()]

        if self.getCurrentFP() < power[0]:
            raise ValueError("not enough FP", self.currentFP, AbilityName)

        self.depleteFP(power[0])
        return power[1] 


    def depleteFP(self, val):

        print "\tdepleting %d by %d" %(self.getCurrentFP(), val)
        self.currentFP -= val



        if self.currentFP > self.maxFP:
            self.currentFP = self.maxFP
        
        if self.currentFP < 0:
            self.currentFP = 0

    def regenFP(self):
        self.currentFP += 1

        if self.currentFP > self.maxFP:
            self.currentFP = self.maxFP
        
        if self.currentFP < 0:
            self.currentFP = 0

        