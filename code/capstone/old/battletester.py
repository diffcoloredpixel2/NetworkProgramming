def tester():
    atuple = (3, 6, 9)
    ytuplelist = [("Force Push", 2, (1, 6, 1))]

    a = Youngling (1, (1,3,0), 10, 20, 3, ytuplelist)
    b = Youngling (1, (1,3,0), 10, 20, 3, ytuplelist)

    print "test hp"
    print a.getCurrentHP()
    a.takeDamage(10)
    print a.getCurrentHP()
    a.heal(8)
    print a.getCurrentHP()
    a.heal(8)
    print a.getCurrentHP()

    print "test fp"
    print a.getCurrentFP()
    a.useForceAbility("force push")
    a.useForceAbility("force push")
    a.regenFP()
    print a.getCurrentFP()
    a.useForceAbility("force push")
    print a.getCurrentFP()
    a.regenFP()

    print calculateDamage(atuple)