from random import randint

def roll(cnt, dice):
    dicesum = 0
    #print "\trolling...",
    for i in xrange(0, cnt):
        x = randint(1, dice)
        dicesum += x
        #print x,
    #print 
    #print "\tdicesum = %d" %(dicesum)
    return dicesum

def calculateDamage(dmgTuple):
    #print "%dD%d + %d" %(dmgTuple[0], dmgTuple[1], dmgTuple[2])
    return roll(dmgTuple[0], dmgTuple[1]) + dmgTuple[2]

def makeMove(char):
    forceMoves = char.getForceAbilities().keys()

    x = randint(0, len(forceMoves) + 1)

    try:
        return forceMoves[x]

    except IndexError:
        return 'REGULAR ATTACK'


