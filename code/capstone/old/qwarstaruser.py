userdb = 'data/user.db'

keyname = ['alignment', 'xp']

def register(username, password, alignment):
	try:
		fd = open(userdb, 'a')

		line =  "%s:%s:%s:%d\n" %(username, password, alignment, 0)
		fd.write(line)
		fd.close()

		print "\tRegistered: " + line
		return "Success"
	except:
		print "Register error: (%s, %s, %s, %d)"  %(username, password, alignment, 0)
		return "Registration Failed"

def login(username, password):
	fd = open(userdb, 'r')
	lines = fd.readlines()

	for line in lines:
		#find the user
		if line.startswith(username):
			explode = line.split(':')
			#validate password
			if explode[1] == password :
				return { 'Result': 'Success', 'Alignment': explode[2], 'XP': int(explode[3]), 'Name': username }
	return {'Result': 'Invalid User or Password'}
