from forceuser import ForceUser

class Youngling (ForceUser):
    def __init__(self=None, toHit=1, dmg=(1,3,0), defense=10, hp=20, fp=3, abilityList={} ):
        abilityList.update( {"FORCE PUSH": (2, (1,6,0))} )
        ForceUser.__init__(self, toHit, dmg, defense, hp, fp, abilityList)

class Padawan (Youngling):
    def __init__(self=None, toHit=3, dmg=(1,6,1), defense=12, hp=30, fp=4, abilityList={}):
        abilityList.update( { "HEAL": (3, (2, 6, 6)) } )
        Youngling.__init__(self, toHit, dmg, defense, hp, fp, abilityList)

class JediKnight (Padawan):
    def __init__(self=None, toHit=6, dmg=(2,6,2), defense=18, hp=50, fp=6, abilityList={}):
        abilityList.update( { "LIGHTSABER FLURRY": (5, (3, 6, 6)) } )
        Padawan.__init__(self, toHit, dmg, defense, hp, fp, abilityList)

class JediMaster (JediKnight):
    def __init__(self=None, toHit=12, dmg=(3,6,3), defense=22, hp=75, fp=8, abilityList={}):
        abilityList.update( { "DRAIN FORCE POINTS": (7, (None, None, None)) } )
        JediKnight.__init__(self, toHit, dmg, defense, hp, fp, abilityList)
