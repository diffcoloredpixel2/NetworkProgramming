import socket
import struct

packed = struct.pack('!HIhi', 1,2,-3,-4)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect( ('127.0.0.1', 1337))
s.send(packed)
s.close()