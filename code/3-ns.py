from raw_socket_helper import RawSocket_IPv6

interfaceName = "ens33"
#interfaceName = "lo"
#interfaceName = "enp0s3"
#interfaceName = "eth0"
raw_socket = RawSocket_IPv6(interfaceName)


srcmac = b'\x00\x0c\x29\x30\x51\x17' 
srcip = b'\x00\x0a\x00\x0c\x00\x07\x00\x09\x00\x00\x00\x00\x00\x00\x00\x05' 

dstmac = b'\x33\x33\x00\x00\x00\x01' # layer 2 IPv6Mcast
dstip = b'\x00\x0a\x00\x0c\x00\x07\x00\x09\x00\x00\x00\x00\x00\x00\x00\x20' 

#icmp v6 options. RFC2461
icmpv6opt = "\x01"      #type 1 = src link-layer addr
icmpv6opt += '\x01'     #len = 8
icmpv6opt += srcmac


#icmp v6
icmpv6hdr = '\x87'          # type, neighbor solicitation
icmpv6hdr += '\x00'         # code: 0
icmpv6hdr += '\x00\x00'     # checksum calculated by helper code
icmpv6hdr += '\x00'*4       # reserved
icmpv6hdr += dstip          # IPv6 addr we need a Ethernet address for
icmpv6 = icmpv6hdr + icmpv6opt


# IPv6 Header (aka Ethernet Payload)
ipv6hdr = '\x60\x00\x00\x00'    # ver, traffic class, flow label (odd byte alignment if broken apart)
ipv6hdr += '\x00\x20'           # size of ICMPv6 Header and any options
ipv6hdr += '\x3a'               # next header
ipv6hdr += '\xff'               # hop limit
ipv6hdr += srcip
ipv6hdr += dstip
ipv6hdr += icmpv6

# Ethernet Header
dst = dstmac
src = srcmac
typ = '\x86\xdd'    # 0x0800 is IPv4, 0x0806 is ARP, 0x86dd is IPv6

etherhdr = dst + src + typ

frame = etherhdr + ipv6hdr

for i in range(1):
    raw_socket.send_chksum(frame)
