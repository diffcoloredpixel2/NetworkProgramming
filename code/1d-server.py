import socket
import struct

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('',1337))
s.listen(1)
s2, q = s.accept()
data = s2.recv(1024)
s2.close()
s.close()

unpackedbig = struct.unpack('>HIhi', data)
print unpackedbig

unpackedlittle = struct.unpack('<HIhi', data)
print unpackedlittle