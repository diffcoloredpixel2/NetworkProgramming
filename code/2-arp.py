from raw_socket_helper import RawSocket

interfaceName = "ens33"
interfaceName = "enp0s3"
raw_socket = RawSocket(interfaceName)

"""
from ifconfig:

ens33     Link encap:Ethernet  HWaddr 00:0c:29:c9:29:bd  
          inet addr:192.168.1.109  Bcast:192.168.1.255  Mask:255.255.255.0
          inet6 addr: fe80::20c:29ff:fec9:29bd/64 Scope:Link
          inet6 addr: a:c:7:9::90/64 Scope:Global
"""

srcmac = '\x08\x00\x27\x37\xad\x8f' # real hardware address of interface
srcip = "\x0a\x00\x2a\x2f"          # src ip 

dstmac = "\xff" * 6         # broadcast mac
dstip = "\x0a\x00\x2a\x01"  # dst ip 

# Ethernet Header
dst = dstmac
src = srcmac
typ = "\x08\x06"    # 0x0800 is IPv4, 0x0806 is ARP

etherhdr = dst + src + typ

# ARP Header 
arpreq = "\x00\x01"     # type (ethernet = 1)
arpreq += "\x08\x00"    # protocol type (IPv4 = 0x0800)
arpreq += "\x06"        # hardware addr len bytes (Ethernet Address are 6 bytes)
arpreq += "\x04"        # protocol addr len bytes (IPv4 Address are 4 bytes)
arpreq += "\x00\x01"    # operation code (1 is request, 2 is reply)
arpreq += srcmac        # src hardware addr
arpreq += srcip         # src IPv4 address
arpreq += "\x00"*6      # dst hardware addr is all 0's, we don't know it
arpreq += dstip         # dst IPv4

frame = etherhdr + arpreq # The completed frame

count = 5   # Makes it easy to find in wireshark
for i in range(count):
    raw_socket.send(frame)

#Straight Raw Sockets
#s = socket.socket(socket.PF_PACKET, socket.SOCK_RAW)
##s.bind((interfaceName, 0))
#for i in range(count):
#    frame = frame #+ "\x00" * i
#    s.send(frame)
#s.close()
