import socket
import time

###########################################################
#
#   Lab 4 Task 2 - Multicast Client
#
#       Client that can send UDP datagrams on
#       Multicast address
#
###########################################################

# Configure address info for connection
dst_ip = "FF02::111:b"
dst_ip_def = "FF02::1"
dst_port = 7777
# Create and configure UDP socket (DGRAM)
my_sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
my_sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_HOPS, 5)

for i in range(1,10000):
    data = ("Sending packet #%d on  %s" % (i,dst_ip))
    my_sock.sendto(data, (dst_ip, dst_port))

    data = ("Sending packet #%d on  %s" % (i,dst_ip_def))
    my_sock.sendto(data, (dst_ip_def, dst_port))

    time.sleep(3)
