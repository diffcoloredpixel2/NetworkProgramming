import struct

# Type is unsigned int of 1 byte, = 135
# Code is unsigned int of 1 byte, = 0
# checksum is an unsigned int of 2 bytes = 0

print "ICMP Header No Options"
pdata = struct.pack('!BBH', 135, 0, 0)
print pdata
print repr(pdata)
print struct.calcsize('BBH')

udata = struct.unpack('BBH', pdata)
print udata


print "\nTCP Header"
# CWR, ECN, URG, ACK, PUSH, RST, SYN, FIN
urg_ack_psh = 56 #00111000 in binary

pdata = struct.pack('!HHIIBBHHH', 8888, 55325, 3724380241, 2245956259, 0x50, 56, 0x1e5, 0, 0)
print pdata
print repr(pdata)
print struct.calcsize('!HHIIBBHHH')
udata = struct.unpack('!HHIIBBHHH', pdata)
print udata
