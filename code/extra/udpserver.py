import socket   				# Import socket module

recvsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  	# Create a socket object

myport = 6789  					# Port for your server
myhost = '127.0.0.1'  			# Your IP to listen on
b = (myhost, myport)			# The Python function bind() requires a tuple of host,port 
recvsock.bind(b)				# Bind to the port

while True:
   data, client = recvsock.recvfrom(256)	# recv data

   print 'Connection from ip, port %s. Data: %s' %(client, data)

   recvsock.sendto(data, client)	# send the data back