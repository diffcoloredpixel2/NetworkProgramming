import socket
import os

#socket.getprotobyname("ipv6-icmp"), socket.getprotobyname("icmp")

#icmp_raw_sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
#icmp6_raw_sock.settimeout(1)
#data, addr = icmp_raw_sock.recvfrom(1508)

icmp6_raw_sock = socket.socket(socket.AF_INET6, socket.SOCK_RAW, socket.IPPROTO_ICMPV6)
icmp6_raw_sock.settimeout(1)
cmd = 'ping6 -c 5 ' + 'a:c:7:9::97'
os.system(cmd)
try:
    data, addr = icmp6_raw_sock.recvfrom(1508)
    print "Packet from %r: %r" % (addr,data)
except socket.timeout:
    print "No response"