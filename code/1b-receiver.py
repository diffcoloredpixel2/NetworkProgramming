import socket   				# Import socket module
import json

recvsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  	# Create a socket object
recvsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
myport = 6789  					# Port for your server
myhost = '127.0.0.1'  			# Your IP to listen on
b = (myhost, myport)			# The Python function bind() requires a tuple of host,port 
recvsock.bind(b)				# Bind to the port

while True:
   data, client = recvsock.recvfrom(256)	# recv data

   data2 = json.loads(data)

   print 'Connection from ip, port %s. Data: %s' %(client, data2)