import socket   				# Import socket module
import json
import time

udpsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  	# Create a socket object
udpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

myport = 10000                   # Port for your server
myhost = '127.0.0.1'            # Your IP to listen on
b = (myhost, myport)            # The Python function bind() requires a tuple of host,port 
udpsock.bind(b)                # Bind to the port

while True:
    data, client = udpsock.recvfrom(256)   # recv data
    print 'Connection from ip, port %s. Data: %s' %(client, data)
